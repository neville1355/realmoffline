﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCI32
{
    public enum SCI32ResourceType : byte
    {
        V56 = 0x0,
        P56 = 0x1,
        Scr = 0x2,
        Txt = 0x3,
        Snd = 0x4,
        Etc = 0x5,
        Voc = 0x6,
        Fon = 0x7,
        Cur = 0x8,
        Pat = 0x9,
        Bmp = 0xA,
        Pal = 0xB,
        Wav = 0xC,
        Aud = 0xD,
        Syn = 0xE,
        Msg = 0xF,
        Map = 0x10,
        Hep = 0x11,
        Chunk = 0x12,
        Audio36 = 0x13,
        Sync36 = 0x14,
        Trans = 0x15,
        Robot = 0x16,
        Vmd = 0x17,
        None = 0x1A,
        EndResource = 0xFF
    }
}
