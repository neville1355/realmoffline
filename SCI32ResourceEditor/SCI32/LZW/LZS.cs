﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
namespace SCI32.LZW
{
    public class LZS
    {
        unsafe char* s;
        unsafe char* d;
        ulong srcpos;

        public unsafe string Res()
        {
            return Marshal.PtrToStringAnsi((IntPtr)d);
        }
        ulong getrevbits(int numbits)
        {
            ulong result = 0;
            if (numbits > 0)
            {
                int bytpos = (int)srcpos / 8;
                int bitpos = (int)srcpos % 8;
                unsafe
                {
                    result = (ulong)((s[bytpos] << 16) | (s[bytpos + 1] << 8) | s[bytpos + 2]);
                    result = (ulong)((result >> (24 - numbits - bitpos)) & (ulong)((1L << numbits) - 1));
                    srcpos += (ulong)numbits;
                }
            }
            return result;
        }
        int getlen()
        {
            int bits;
            int length = 2;
            do
            {
                bits = (int)getrevbits(2);
                length += bits;
            }
            while ((bits == 3) && (length < 8));

            if (length == 8)
                do
                {
                    bits = (int)getrevbits(4);
                    length += bits;
                }
                while (bits == 15);

            return length;
        }
        public unsafe ulong lzs_unpack(string p, string u)
        {
            char* ppp, uuu;

            fixed (char* pp = p)
            {
                s = pp;
                ppp = pp;
            }
            fixed(char* uu = u)
            {
                d = uu;
                uuu = uu;
            }

            srcpos = 0;

            do
            {
                int tag = (int)getrevbits(1);
                if (tag == 0)                   // Uncompressed byte
                    *d++ = (char)getrevbits(8);
                else                        // Chain
                {
                    int ofs;
                    tag = (int)getrevbits(1);
                    if (tag == 1)               // 7-bit offset or END
                    {
                        ofs = (int)getrevbits(7);
                        if (ofs == 0)           // END of stream
                            break;
                    }
                    else                    // 11-bit offset
                        ofs = (int)getrevbits(11);
                    int len = getlen();
                    char* dic = &d[-ofs];
                    if (dic < uuu)
                    {

                        break;
                    }
                    while (len-- != 0)
                        *d++ = *dic++;
                }
            }
            while (true);                      //Until breaked

            return (ulong)d - (ulong)uuu;
        }
    }
}
