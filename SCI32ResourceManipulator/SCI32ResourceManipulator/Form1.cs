﻿using SCI32Lib;
using SCI32Lib.Classes;
using SCI32Lib.Compression;
using SCI32Lib.Stucts;
using SCI32Lib.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SCI32ResourceManipulator
{
    public partial class Form1 : Form
    {
        public Bitmap LastOpenBitmap;

        public Form1()
        {
            InitializeComponent();
            openBMPToolStripMenuItem.Visible = false;
        }
        #region File Menu
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "SCI32 View (*.v56)|*.v56|SCI32 Message (*.msg)|*.msg|SCI32 Picture File (*.P56)|*.p56|Bitmap Files (*.bmp)|*.bmp|PNG Files (*.png)|*.png|Jpg Files|*.jpg";
            open.FilterIndex = 3;
            DialogResult result = open.ShowDialog();

            if (result == DialogResult.OK)
            {
                // get extention
                string ext = InternalTools.GetFileExtention(open.FileName);
                string fileName = InternalTools.GetWorkigFileName(open.FileName);

                Bitmap bmp = null;

                switch (ext.ToLower())
                {
                    case "p56":
                        {
                            PictureInformation pInfo = null;
                            InternalTools.ReadP56(File.ReadAllBytes(open.FileName), open.FileName, out bmp, out pInfo);
                            imageInformationControl1.PopulateData(pInfo);
                            LastOpenBitmap = bmp;
                        }
                        break;
                    case "v56":
                        {
                            Color[] colors = null;
                            InternalTools.ReadV56(open.FileName, out colors);

                            if (colors != null) paletteOriginal.ShowPalette(colors);
                        }
                        break;
                    case "jpg":
                    case "png":
                    case "bmp":
                        {
                            Image img = Image.FromFile(open.FileName);
                            

                            if(img.Size.Width != 640 && img.Size.Height != 480)
                            {
                                Image i = img;

                                Bitmap resize = ResizeImage(i);
                                img = resize;
                            }
                            // This works, comes close to palette, good enough for gov work.
                            Bitmap b = BitmapConvertor.Quantize(img);
                            LastOpenBitmap = b;
                            //MessageBox.Show(string.Format("Bitmap is now {0} Format.", b.PixelFormat));
                            picBoxOriginal.Image = b;
                            paletteOriginal.ShowPalette(b.Palette.Entries);
                        }
                        break;
                    case "msg":
                        {
                            MemoryStream _stream = new MemoryStream(File.ReadAllBytes(open.FileName));
                            BinaryReader reader = new BinaryReader(_stream);

                            MessageFileHeader mfh = InternalTools.BinaryReaderToStruct<MessageFileHeader>(reader);
                            StringBuilder b = new StringBuilder();
                            b.AppendLine(string.Format("File {0}", open.FileName));
                            b.AppendLine(string.Format("Message Tag {0}", mfh.msgTag.ToString("X2")));
                            b.AppendLine(string.Format("Sci Type Tag {0}", mfh.sciTag.ToString("X2")));
                            b.AppendLine(string.Format("Unknown SHort1 {0}", mfh.unkShort1));
                            b.AppendLine(string.Format("Unknown Short2 {0}", mfh.unkShort2));
                            b.AppendLine(string.Format("Phrases Count {0}", mfh.phraseCount));
                            b.AppendLine(string.Format("End of Messages Offset {0}", mfh.messagesLength));

                            MessageBox.Show(string.Format(b.ToString()));

                        }
                        break;
                }

                if (bmp != null)
                {
                    picBoxOriginal.Image = bmp;
                    paletteOriginal.ShowPalette(bmp.Palette.Entries);
                }
                
            }
        }
      

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (picBoxOriginal.Image == null) return;
            SaveFileDialog save = new SaveFileDialog();
            save.AddExtension = true;
            save.Filter = "SCI32 View (*.P56)|*.p56|Bitmap Files (*.bmp)|*.bmp|PNG Files (*.png)|*.png";

            DialogResult result = save.ShowDialog();

            if (result == DialogResult.OK)
            {
                string ext = InternalTools.GetFileExtention(save.FileName);
                string fileName = InternalTools.GetWorkigFileName(save.FileName);

                switch (ext)
                {
                    case "p56":
                        {
                            if (LastOpenBitmap == null) return;
                            if (LastOpenBitmap.PixelFormat != PixelFormat.Format8bppIndexed)
                            {
                                MessageBox.Show(string.Format("{0} is a incorrect format !", LastOpenBitmap.PixelFormat));
                                return;
                            }

                          //  InternalTools.WriteSci(save.FileName, LastOpenBitmap);
                           // Image img = null;
                            //InternalTools.NewWriteSci(save.FileName, picBoxOriginal.Image, out img);

                            //if (img != null) picBoxSCI32.Image = img;
                            
                            PictureInformation pInfo = null;
                            InternalTools.WriteSCI32P56File(save.FileName, picBoxOriginal.Image, out pInfo);
                            // Now open the new file in secondary boxes.
                           // Bitmap bmp = null;
                            //PictureInformation pInfo2 = null;
                           // InternalTools.ReadP56(pInfo.Cell.SCIFile, save.FileName , out bmp, out pInfo2);
                           // imageInformationControl2.PopulateData(pInfo2);
                          //  picBoxSCI32.Image = pInfo.Cell.Image;
                           // paletteSCI32.ShowPalette(pInfo.Cell.Image.Palette.Entries);
                            
                        }
                        break;
                    case "bmp":
                        {
                            if (picBoxOriginal.Image != null) picBoxOriginal.Image.Save(save.FileName, ImageFormat.Bmp);
                        }
                        break;
                }

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        #endregion
        #region Picture DbleClick
        private void picBoxOriginal_Click(object sender, EventArgs e)
        {
            // get the Image
            Image img = picBoxOriginal.Image;
        }

        private void picBoxSCI32_Click(object sender, EventArgs e)
        {
            // get te Image
            Image img = picBoxSCI32.Image;
        }
        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {
            this.paletteOriginal.MouseMove += PaletteOriginal_MouseMove;
        }

        public static Bitmap ResizeImage(Image imgToResize)
        {
            Size p56 = new Size(640, 480);
            return new Bitmap(imgToResize, p56);
        }

        private void PaletteOriginal_MouseMove(object sender, MouseEventArgs e)
        {
            Point mpoint = paletteOriginal.PointToClient(Control.MousePosition);
            Rectangle mouse = new Rectangle(mpoint, new Size(2, 2));
            // Annoying way to do it, but it stops the flashing and works, so i am over it :D
            for (int i = 0; i < paletteOriginal.PaletteRects.Count; i++)
            {
                if (paletteOriginal.PaletteRects.ElementAt(i).Key.IntersectsWith(mouse))
                {
                    string palEntry = string.Format("Palette Entry({0})", i + 1);
                    if (paletteOriginal.LastToolTipReport == null)
                    {
                        paletteOriginal.LastToolTipReport = paletteOriginal.PaletteRects.ElementAt(i).Key;
                    }
                    else if (paletteOriginal.LastToolTipReport != paletteOriginal.PaletteRects.ElementAt(i).Key)
                    {
                        ToolTip t = new ToolTip(new System.ComponentModel.Container());
                        t.ToolTipTitle = palEntry;
                        t.SetToolTip(paletteOriginal, paletteOriginal.PaletteRects.ElementAt(i).Value.ToString());
                        paletteOriginal.LastToolTipReport = paletteOriginal.PaletteRects.ElementAt(i).Key;
                    }
                }
            }
        }

        private void paletteSCI32_Load(object sender, EventArgs e)
        {

        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "BMP Files|*.bmp";

            DialogResult result = open.ShowDialog();

            if(result == DialogResult.OK)
            {
                MemoryStream _stream = new MemoryStream(File.ReadAllBytes(open.FileName));
                BinaryReader _reader = new BinaryReader(_stream);

                BITMAPINFO bmpInfo = InternalTools.BinaryReaderToStruct<BITMAPINFO>(_reader);

                List<RGBQUAD> quads = new List<RGBQUAD>();

                for(int i = 0;i< bmpInfo.bmiImageHeader.colorsUsed;i++)
                {
                    RGBQUAD rgb = InternalTools.BinaryReaderToStruct<RGBQUAD>(_reader);
                    quads.Add(rgb);
                }

                byte[] data = _reader.ReadBytes(bmpInfo.bmiHeader.bmpSize);
                StringBuilder b = new StringBuilder();

                b.AppendLine(string.Format("BMP Type {0}", Encoding.ASCII.GetString(BitConverter.GetBytes(bmpInfo.bmiHeader.bmpType))));
                b.AppendLine(string.Format("BMP Image Size {0}", bmpInfo.bmiHeader.bmpSize));
                b.AppendLine(string.Format("BMP Pixel Data Offset {0}", bmpInfo.bmiHeader.pixelDataOffset));
                b.AppendLine(string.Format("BMP Bits Per Pixel {0}", bmpInfo.bmiImageHeader.bitsPerPixel));
                b.AppendLine(string.Format("BMP Colors Used {0}", bmpInfo.bmiImageHeader.colorsUsed));
                b.AppendLine(string.Format("BMP Header Size {0}", bmpInfo.bmiImageHeader.headerSize));
                b.AppendLine(string.Format("BMP Image Compression {0}", bmpInfo.bmiImageHeader.imageCompression));
                b.AppendLine(string.Format("BMP Image Height {0}", bmpInfo.bmiImageHeader.imageHeight));
                b.AppendLine(string.Format("BMP Image Width {0}", bmpInfo.bmiImageHeader.imageWidth));
                b.AppendLine(string.Format("BMP Image Planes {0}", bmpInfo.bmiImageHeader.imagePlanes));
                b.AppendLine(string.Format("BMP Image Size {0}", bmpInfo.bmiImageHeader.imageSize));
                b.AppendLine(string.Format("BMP Important Colors {0}", bmpInfo.bmiImageHeader.importantColors));
                b.AppendLine(string.Format("BMP X Pixels Per Meter {0}", bmpInfo.bmiImageHeader.xPixelsPerMeter));
                b.AppendLine(string.Format("BMP Y Pixels Per Meter {0}", bmpInfo.bmiImageHeader.yPixelsPerMeter));
                b.AppendLine(string.Format("At Offset {0} file length {1}", _stream.Position, _stream.Length));
                b.AppendLine(string.Format("Width times height = {0} bytes, we have {1} bytes for data", 
                    bmpInfo.bmiImageHeader.imageWidth * bmpInfo.bmiImageHeader.imageHeight, (data.Length + 1024)));
                List<Color> colors = new List<Color>();
                for(int i = 0;i<quads.Count;i++)
                {
                    Color c = Color.FromArgb(quads[i].res, quads[i].red, quads[i].green, quads[i].blue);
                    colors.Add(c);
                }
                Image old = Bitmap.FromFile(open.FileName);

                b.AppendLine(string.Format("Old Pixel Format {0} with {1} loaded colors", old.PixelFormat, colors.Count));

                Bitmap bmp = InternalTools.CopySCI32DataToBitmap(data, colors.ToArray(), bmpInfo.bmiImageHeader.imageWidth, bmpInfo.bmiImageHeader.imageHeight);

               // byte[] decompress = QuickLZ.decompress(data);

                //if (bmp != null) picBoxOriginal.Image = bmp;
                MessageBox.Show(b.ToString());
            }
        }
        public void SizeCheckV56()
        {
            StringBuilder b = new StringBuilder();
            b.AppendLine(string.Format("ViewCellHeader should be 36 its {0}", Marshal.SizeOf(typeof(ViewCellHeader))));

            MessageBox.Show(b.ToString());
        }

        private void checkSizesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SizeCheckV56();
        }

        private void extractPaletteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (picBoxOriginal.Image == null) return;
            if(picBoxOriginal.Image.Palette.Entries.Length == 0)
            {
                MessageBox.Show("Picture has no 256 color Palette !!");
                return;
            }

            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Microsoft Palette Files (*.pal)|*.pal";

            DialogResult result = save.ShowDialog();

            if (result == DialogResult.OK)
            {
                try
                {
                    PaletteTools.SaveMicrosoftPalette(save.FileName, picBoxOriginal.Image.Palette.Entries);
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void swapPaletteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (picBoxOriginal.Image == null) return;
            if (picBoxOriginal.Image.Palette.Entries.Length == 0)
            {
                MessageBox.Show("Picture has no 256 color Palette to swap!!");
                return;
            }

            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Microsoft Palette Files (*.pal)|*.pal";

            DialogResult result = open.ShowDialog();

            if(result == DialogResult.OK)
            {
                try
                {
                    Color[] colors = PaletteTools.LoadMicrosoftPalette(File.ReadAllBytes(open.FileName));
                    ColorPalette p = picBoxOriginal.Image.Palette;

                    for(int i = 0;i<colors.Length;i++)
                    {
                        p.Entries.SetValue(colors[i], i);
                    }

                    picBoxOriginal.Image.Palette = p;
                    picBoxOriginal.Refresh();
                    paletteOriginal.ShowPalette(colors);
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }
    }
}
