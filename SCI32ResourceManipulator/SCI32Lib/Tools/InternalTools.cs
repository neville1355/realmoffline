﻿using SCI32Lib.Classes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SCI32Lib.Tools
{
    public static class InternalTools
    {
        #region Tools
        /// <summary>
        /// Returns just the extention, no preceeding .
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static string GetFileExtention(string fullPath)
        {
            return fullPath.Split('.').Last();
        }
        /// <summary>
        /// Retunrs file name minus path
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static string GetWorkigFileName(string fullPath)
        {
            return fullPath.Split('\\').Last();
        }
        public static T ByteArrayToStructure<T>(byte[] bytes) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(),
                typeof(T));
            handle.Free();
            return stuff;
        }
        public static T BinaryReaderToStruct<T>(BinaryReader br) where T : struct
        {
            //Read byte array
            byte[] buff = br.ReadBytes(Marshal.SizeOf(typeof(T)));
            //Make sure that the Garbage Collector doesn't move our buffer 
            GCHandle handle = GCHandle.Alloc(buff, GCHandleType.Pinned);
            //Marshal the bytes
            T s =
              (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(),
              typeof(T));
            handle.Free();//Give control of the buffer back to the GC 
            return s;
        }
        public static byte[] StructureToByteArray(object obj)
        {
            int len = Marshal.SizeOf(obj);

            byte[] arr = new byte[len];

            IntPtr ptr = Marshal.AllocHGlobal(len);

            Marshal.StructureToPtr(obj, ptr, true);

            Marshal.Copy(ptr, arr, 0, len);

            Marshal.FreeHGlobal(ptr);

            return arr;
        }
        public static byte[] BitmapToByteArray(Bitmap bitmap)
        {
            if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) throw new Exception(string.Format("Incorrect pixel format of {0}!!", bitmap.PixelFormat));
            BitmapData bmpdata = null;

            try
            {
                bmpdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
                int numbytes = bmpdata.Stride * bitmap.Height;
                byte[] bytedata = new byte[numbytes];
                IntPtr ptr = bmpdata.Scan0;

                Marshal.Copy(ptr, bytedata, 0, numbytes);

                return bytedata;
            }
            finally
            {
                if (bmpdata != null)
                    bitmap.UnlockBits(bmpdata);
            }

        }
       
      
        public static SCI32RGBQuad[] ConvertColorsToSCI32RGB(Color[] colors)
        {
            List<SCI32RGBQuad> result = new List<SCI32RGBQuad>();

            for(int i = 0;i< colors.Length;i++)
            {
                SCI32RGBQuad q = new SCI32RGBQuad();
                q.Initialize(colors[i]);

                result.Add(q);
            }
            return result.ToArray();
        }
        public static void WriteSCI32RGBQuadToBinaryWriter(SCI32RGBQuad[] quads, BinaryWriter writer)
        {
            for(int i=0; i< quads.Length;i++)
            {
                writer.Write(StructureToByteArray(quads[i]));
            }
        }
        #endregion

        #region Bitmap Creation raw bmp data
        /// <summary>
        /// function CopyDataToBitmap
        /// Purpose: Given the pixel data return a bitmap of size [width,height],PixelFormat=8RGB 
        /// </summary>
        /// <param name="data">Byte array with pixel data</param>
        public static System.Drawing.Bitmap CopySCI32DataToBitmap(byte[] data, Color[] colors, int width, int height)
        {
            //Here create the Bitmap to the known height, width and format
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(width, height, PixelFormat.Format8bppIndexed);

            
            ColorPalette p = bmp.Palette;

           // System.Windows.Forms.MessageBox.Show(string.Format("We have {0} we want to add, bmp was created with {1} entries we can use.", colors.Length, p.Entries.Length));

            // clear out palette
            for (int i = 0; i< p.Entries.Length; i++)
            {
                p.Entries.SetValue(Color.Black, i);
            }
            // Ok so we need to start at 0 then black out until first color, then if we still not at 256 black out until end.
            
            for (int i = 0; i < colors.Length; i++)
            {
                p.Entries.SetValue(colors[i], i);
            }
            bmp.Palette = p;

            //bmp.Palette = colors;
            //Create a BitmapData and Lock all pixels to be written 
            BitmapData bmpData = bmp.LockBits(
                                 new Rectangle(0, 0, bmp.Width, bmp.Height),
                                 ImageLockMode.WriteOnly, bmp.PixelFormat);

            //Copy the data from the byte array into BitmapData.Scan0
            Marshal.Copy(data, 0, bmpData.Scan0, data.Length);


            //Unlock the pixels
            bmp.UnlockBits(bmpData);


            //Return the bitmap 
            return bmp;
        }
        public static Color[] FixPaletteColors(Color[] input, short firstColor)
        {
            List<Color> fixedColors = new List<Color>(256);

            if (input.Length == 256) return input;

            if (input.Length + firstColor > 256) throw new ArgumentOutOfRangeException(string.Format("First color or color list are out of range, 256 is max value, you tried to give {0}", input.Length + firstColor));

            // here we go

            for(int i = 0;i< firstColor;i++)
            {
                fixedColors.Add(Color.Black);
            }
            for(int i = firstColor; i< input.Length;i++)
            {
                fixedColors.Add(input[i]);
            }

            for(int i = fixedColors.Count; i<256;i++ )
            {
                fixedColors.Add(Color.Black);
            }
            return fixedColors.ToArray(); // we are now a 256 color palette

        }
        public static Color[] ConvertSCIRGBQuadToColors(SCI32RGBQuad[] sciColors)
        {
            List<Color> result = new List<Color>();
            for (int i = 0; i < sciColors.Length; i++)
            {
                Color c = Color.FromArgb(255, sciColors[i].red, sciColors[i].green, sciColors[i].blue);
                result.Add(c);
            }
            return result.ToArray();
        }
        public static SCI32RGBQuad ConvertBMPQuadToSCIQuad(BMPRGBQuad bmpQuad)
        {
            // we reverse
            SCI32RGBQuad result = new SCI32RGBQuad();
            result.res = bmpQuad.res;
            result.red = bmpQuad.red;
            result.green = bmpQuad.green;
            result.blue = bmpQuad.blue;
            return result;
        }
        public static BMPRGBQuad ConvertSCIQuadToBMPQuad(SCI32RGBQuad sciQuad)
        {
            // we reverse
            BMPRGBQuad result = new BMPRGBQuad();
            result.res = sciQuad.res;
            result.red = sciQuad.red;
            result.green = sciQuad.green;
            result.blue = sciQuad.blue;
            return result;
        }
        public static Bitmap ConvertImageTo256Colors(Image img, PixelFormat format = PixelFormat.Format8bppIndexed)
        {
            Bitmap bmp = new Bitmap(img);
            return bmp.Clone(new Rectangle(0, 0, bmp.Width, bmp.Height), format);
        }
        public static Color[] Fix256PaletteForSCI(Color[] colors)
        {
            List<Color> result = new List<Color>();

            for(int i = 0;i<colors.Length;i++)
            {
                if (colors[i] != Color.FromArgb(255, 0, 0, 0)) result.Add(colors[i]);
            }
            return result.ToArray();
        } 
        #endregion
       
        #region Read P56
        public static void ReadP56(byte[] file, string filename, out Bitmap bmp, out PictureInformation pictureInfo)
        {
            MemoryStream stream = new MemoryStream(file);
            BinaryReader reader = new BinaryReader(stream);
            bmp = null;


            P56InfoHeader32 info = BinaryReaderToStruct<P56InfoHeader32>(reader);


            CellHeader cellhead = BinaryReaderToStruct<CellHeader>(reader);

            PalHeaderInfo palInfoHeader = BinaryReaderToStruct<PalHeaderInfo>(reader);

            if (palInfoHeader.paletteTag != 0x0300) System.Windows.Forms.MessageBox.Show(string.Format("Palette Tag should be 0x0300 it is {0} instead !!", palInfoHeader.paletteTag.ToString("X2")));

            PalHeader palHeader = BinaryReaderToStruct<PalHeader>(reader);
            

            // Now get colors ?
            List<SCI32RGBQuad> sciColors = new List<SCI32RGBQuad>();

            for (int i = 0; i < palHeader.numColors; i++)
            {
                SCI32RGBQuad sciRGB = BinaryReaderToStruct<SCI32RGBQuad>(reader);
                sciColors.Add(sciRGB);
            }

            SCI32Cell cell = new SCI32Cell();

            byte[] imgBytes = cell.ReadCell(reader, cellhead.Width, cellhead.Height);

            reader.Close();

            Color[] colors = ConvertSCIRGBQuadToColors(sciColors.ToArray());

            cell.SetImage(CopySCI32DataToBitmap(imgBytes, colors, cellhead.Width, cellhead.Height));

            // try this fix
            info._header.Width = cellhead.Width;
            info._header.Height = cellhead.Height;

            pictureInfo = new PictureInformation();
            pictureInfo.P56InfoHeader = info;
            pictureInfo.P56CellHeader = cellhead;
            pictureInfo.P56PaletteInfoHeader = palInfoHeader;
            pictureInfo.P56PaletteHeader = palHeader;
            pictureInfo.PaletteColors = colors;
            pictureInfo.Cell = cell;
            pictureInfo.ImageBytes = imgBytes;
            
            bmp = cell.Image;
        }
        #endregion

        #region Working SCI32 Write
        public static void WriteSCI32P56File(string fileLocation, Image img, out PictureInformation pictureInfo)
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);
            
            
            P56InfoHeader32 info = new P56InfoHeader32();
            info.Initialize(img.Width, img.Height, 1);
            

            CellHeader cellHeader = new CellHeader();
            cellHeader.Initialize(img);

            // Need to force this fix, not sure why
            info._header.Width = cellHeader.Width;
            info._header.Height = cellHeader.Height;
            
            // now write them
            writer.Write(StructureToByteArray(info));
            writer.Write(StructureToByteArray(cellHeader));

            PalHeaderInfo palHeaderInfo = new PalHeaderInfo();
            palHeaderInfo.Initialize(img);

            writer.Write(StructureToByteArray(palHeaderInfo));

            PalHeader palHeader = new PalHeader();
            palHeader.Initialize(img);

            writer.Write(StructureToByteArray(palHeader));
            Color[] fixedColors = InternalTools.Fix256PaletteForSCI(img.Palette.Entries);
            
            SCI32RGBQuad[] quads = ConvertColorsToSCI32RGB(img.Palette.Entries);

            WriteSCI32RGBQuadToBinaryWriter(quads, writer);

            // Now our Image or cell goes here
            SCI32Cell cell = new SCI32Cell((Bitmap)img);

            byte[] imgBytes = null;

            try
            {
                imgBytes = cell.WriteCell(writer);
            }
            catch (Exception ex) { System.Windows.Forms.MessageBox.Show(ex.Message); }
            writer.Close();
            
            pictureInfo = new PictureInformation();
            pictureInfo.P56InfoHeader = info;
            pictureInfo.P56CellHeader = cellHeader;
            pictureInfo.P56PaletteInfoHeader = palHeaderInfo;
            pictureInfo.P56PaletteHeader = palHeader;
            pictureInfo.PaletteColors = ConvertSCIRGBQuadToColors(quads);
            pictureInfo.Cell = cell;
            pictureInfo.ImageBytes = imgBytes;

            pictureInfo.Cell.SCIFile = stream.ToArray();

            try
            {
                File.WriteAllBytes(fileLocation, pictureInfo.Cell.SCIFile);
            }
            catch(Exception ex) { System.Windows.Forms.MessageBox.Show(ex.Message); }
        }
        #endregion

        public static void ReadV56(string fileName, out Color[] palette)
        {
            // byte 49 is cells ?
            MemoryStream _stream = new MemoryStream(File.ReadAllBytes(fileName));
            BinaryReader reader = new BinaryReader(_stream);
            StringBuilder b = new StringBuilder();

            ViewFileHeader vfh = BinaryReaderToStruct<ViewFileHeader>(reader); // 26 bytes

            //System.Windows.Forms.MessageBox.Show(string.Format("Read View File Header should be at position 26 {0}", _stream.Position));
            // good till here

            ViewHeader vh = BinaryReaderToStruct<ViewHeader>(reader);
            long offset = _stream.Position;

            // Seek to palette
            reader.BaseStream.Seek(Marshal.SizeOf(typeof(ViewFileHeader)) + vh.paletteOffset - 6, SeekOrigin.Begin);
            short tag = reader.ReadInt16();
            int pSize = reader.ReadInt32();
            // should be good up until here
           
            
            PalHeader phead = BinaryReaderToStruct<PalHeader>(reader);

            // get colors
            List<SCI32RGBQuad> sciColors = new List<SCI32RGBQuad>();
            // up to first color black it out

            for(int i = 0;i<phead.firstColor;i++)
            {
                SCI32RGBQuad s = new SCI32RGBQuad();
                sciColors.Add(s);
            }

            for (int i = 0;i< phead.numColors;i++)
            {
                SCI32RGBQuad quad = BinaryReaderToStruct<SCI32RGBQuad>(reader);
                sciColors.Add(quad);
            }
            palette = ConvertSCIRGBQuadToColors(sciColors.ToArray());

            // Good up until here, palette loads correctly

            reader.BaseStream.Seek(Marshal.SizeOf(typeof(ViewFileHeader)) + vh.loopTableOffset, SeekOrigin.Begin);

            

            List<LoopHeader> loopHeaders = new List<LoopHeader>();

            short loopTag = reader.ReadInt16(); // should be 0x84

            if (loopTag != 0x84)
            {
                System.Windows.Forms.MessageBox.Show(loopTag.ToString("X2"), "Incorrect Loop tag !!");
            }
           
            // we read the all loop headers here 

            for(int i = 0;i< vh.loopCount;i++)
            {
                LoopHeader lh = BinaryReaderToStruct<LoopHeader>(reader);
                loopHeaders.Add(lh);
            }

            for(int i =0;i<loopHeaders.Count;i++)
            {
                LoopHeader lh = loopHeaders[i];

                _stream.Seek(Marshal.SizeOf(typeof(ViewFileHeader)) + lh.cellsOffset, SeekOrigin.Begin);

                List<ViewCellHeader> _cellHeaders = new List<ViewCellHeader>();

                for(int j = 0;j<lh.numCells;j++)
                {
                    ViewCellHeader vch = BinaryReaderToStruct<ViewCellHeader>(reader);
                    _cellHeaders.Add(vch);
                   // _stream.Seek(vh.cellRecSize - Marshal.SizeOf(typeof(ViewCellHeader)), SeekOrigin.Current);
                }

                for(int k =0;k<_cellHeaders.Count;k++)
                {
                    System.Windows.Forms.MessageBox.Show(string.Format("Compression {0}", _cellHeaders[k].compression));
                    //if (_cellHeaders[i].compression != 0) throw new Exception("Unable to work with compression at this time !! ");

                }
            }
            // correct up to here

            b.AppendLine(string.Format("Patch ID: {0}", vfh.patchId.ToString("X2")));
            b.AppendLine(string.Format("Cell rec Size {0}", vh.cellRecSize));
            b.AppendLine(string.Format("Is Compressed {0}", vh.compressed));
            b.AppendLine(string.Format("Loops {0}", vh.loopCount));
            b.AppendLine(string.Format("Loop rec Size {0}", vh.loopRecSize));
            b.AppendLine(string.Format("Loop Table Offset {0}", vh.loopTableOffset));
            b.AppendLine(string.Format("Number of Cells {0}", vh.numCells));
            b.AppendLine(string.Format("Palette Offset {0}", vh.paletteOffset));
            b.AppendLine(string.Format("Resolution X {0}", vh.resolutionX));
            b.AppendLine(string.Format("Resolution Y {0}", vh.resolutionY));
            b.AppendLine(string.Format("View Size {0}", vh.viewSize));
            
            b.AppendLine(string.Format("Palette Tag Should be 0x0300 it is {0} and palette size is {1}", tag.ToString("X2"), pSize));
            b.AppendLine(string.Format("Palette ID {0}", phead.palID.ToString("X2")));
            b.AppendLine(string.Format("Palette Data length {0}", phead.dataLength));
            b.AppendLine(string.Format("Palette uses 4 colors {0}", phead.exfourColor));
            b.AppendLine(string.Format("Palette First Color {0}", phead.firstColor));
            b.AppendLine(string.Format("Palette Number of Colors {0}", phead.numColors));
            b.AppendLine(string.Format("Palette Uses Triple Colors {0}", phead.tripleColor));
            

            b.AppendLine(string.Format("{0} Loop headers", loopHeaders.Count));

            for(int i = 0; i<loopHeaders.Count;i++)
            {
                LoopHeader h = loopHeaders[i];

                 b.AppendLine(string.Format("Loop {0} Cells offset {1}", i,  h.cellsOffset));
                 b.AppendLine(string.Format("Loop {0} Number of Cells {1}", i, h.numCells));
            }
            System.Windows.Forms.MessageBox.Show(b.ToString());
        }
    }
}
