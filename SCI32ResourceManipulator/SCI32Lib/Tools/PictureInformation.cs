﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCI32Lib
{
    public class PictureInformation
    {
        public P56InfoHeader32 P56InfoHeader;
        public CellHeader P56CellHeader;
        public PalHeaderInfo P56PaletteInfoHeader;
        public PalHeader P56PaletteHeader;
        public Color[] PaletteColors;

        public byte[] ImageBytes;

        public SCI32Cell Cell;

        public PictureInformation()
        {

        }
    }
}
