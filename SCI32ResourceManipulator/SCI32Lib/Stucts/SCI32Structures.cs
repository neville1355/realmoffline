﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SCI32Lib
{
    #region Palette Entry Quads
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BMPRGBQuad
    {
        /// <summary>
        /// The byte value for blue
        /// </summary>
        public byte blue;
        /// <summary>
        /// The byte value for green
        /// </summary>
        public byte green;
        /// <summary>
        /// The byte value for red
        /// </summary>
        public byte red;
        /// <summary>
        /// The byte value for reserved
        /// </summary>
        public byte res;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SCI32RGBQuad
    {
        /// <summary>
        /// The byte value for reserved
        /// </summary>
        public byte res;
        /// <summary>
        /// The byte value for red
        /// </summary>
        public byte red;
        /// <summary>
        /// The byte value for green
        /// </summary>
        public byte green;
        /// <summary>
        /// The byte value for blue
        /// </summary>
        public byte blue;

        /// <summary>
        /// Initialize struct with a specfic color
        /// </summary>
        /// <param name="color"></param>
        public void Initialize(Color color)
        {
            res = 0x01;
            red = color.R;
            green = color.G;
            blue = color.B;
        }
    }
    #endregion

    #region P56SCI32 header Structures
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct P56InfoHeader32 // 18 total bytes
    {
        /// <summary>
        /// 0x00008181 for SCI32
        /// </summary>
        public int _sciType;
        /// <summary>
        /// The P56 header (14 bytes in length)
        /// </summary>
        public P56Header32 _header; // 14 bytes


        /// <summary>
        /// Initialize struct with default values
        /// </summary>
        /// <param name="width">Image Width</param>
        /// <param name="height">Image Height</param>
        /// /// <param name="numberOfCells">The Number of cells containing a image</param>
        public void Initialize(int width, int height, byte numberOfCells)
        {
            _sciType = 0x8181;

            _header.CellRecSize = 0x2A;

            _header.CellsOffset = 14;

            _header.Width = (short)width;

            _header.Height = (short)height;

            _header.PaletteOffset = 62;

            _header.NumCells = numberOfCells;
        }
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct P56Header32
    {
        /// <summary>
        /// Offset of Cells table always 14 for SCI32
        /// </summary>
        public short CellsOffset;
        /// <summary>
        /// Number of cells (1 for views)
        /// </summary>
        public byte NumCells;
        /// <summary>
        /// DRIGO: is compression used
        /// </summary>
        public byte IsCompressed;
        /// <summary>
        /// Size of Cell's table record (0x2A)
        /// </summary>
        public short CellRecSize;
        /// <summary>
        /// Offset of Palette structure
        /// </summary>
        public int PaletteOffset;
        /// <summary>
        /// if Height==0 : 0-320x200, 1-640x480, 2-640x400
        /// </summary>
        public short Width;
        /// <summary>
        /// if Height==0 then Width = (0-320x200, 1-640x480, 2-640x400)
        /// </summary>
        public short Height;
    }
    #endregion

    #region CellHeader Structure
    [StructLayout(LayoutKind.Sequential, Pack = 1)] // total size 42 (0x2A)
    public struct CellHeader
    {
        /// <summary>
        /// Width of bmp picture
        /// </summary>
        public short Width;

        /// <summary>
        ///  height of bmp picture
        /// </summary>
        public short Height;

        /// <summary>
        /// Seems to be the same as width in some cases, others not
        /// </summary>
        public short XShift;

        /// <summary>
        /// Seems to be the same as height in some cases, others not
        /// </summary>
        public short YShift;

        /// <summary>
        /// The transparent color most always 255
        /// </summary>
        public byte TransparentClr;

        /// <summary>
        /// 0 - none, 8A - rle, we use 0
        /// </summary>
        public byte Compression;

        /// <summary>
        /// If Compressed, Size of compressed image + size of pack image, if uncompressed, size of image block only
        /// </summary> 
        public int ImageandPackSize;

        /// <summary>
        /// &0x80 -> &1 - UseSkip, &2 - remap_status
        /// </summary>
        public short Flags;

        /// <summary>
        /// Size of compressed image
        /// </summary>
        public int ImageSize;

        /// <summary>
        /// Where the length of the palheader + colors begins, right after palette tag
        /// </summary>
        public int PaletteOffs;

        /// <summary>
        /// Offset of Cell's image or pack data tags (if compressed)
        /// </summary>
        public int ImageOffs;

        /// <summary>
        /// Offset of Pack data image 
        /// </summary>
        public int PackDataOffs;

        /// <summary>
        /// Offset of Scan Lines Table (if compressed)
        /// </summary>
        public int LinesOffs;

        /// <summary>
        /// not sure, we do not use
        /// </summary>
        public short ZDepth; //-> -1000 - normal bg

        /// <summary>
        /// not sure, we do not use
        /// </summary>
        public short XPos;

        /// <summary>
        /// Not sure, we do not use
        /// </summary>
        public short YPos;


        /// <summary>
        /// Initialize struct with default values
        /// </summary>
        /// <param name="bmp"></param>
        public void Initialize(Image bmp)
        {
            Width = (short)bmp.Width;
            Height = (short)bmp.Height;
            ImageOffs = 1129;
            TransparentClr = 255;
            Flags = 4;
        }
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)] // total size 36 (0x24)
    public struct ViewCellHeader
    {
        public short imgWidth;
        public short imgHeight;
        public short xShift;
        public short yShift;
        public byte tranparentColor;
        public byte compression;
        public short flags;
        public int imageAndPackSize;
        public int imageSize;
        public int paletteOffset;
        public int imageOffset;
        public int packedDataOffset;
        public int linesOffset;
    }
        #endregion

    #region Palette Header Structures
        [StructLayout(LayoutKind.Sequential, Pack = 1)] // total size 6
    public struct PalHeaderInfo
    {
        /// <summary>
        /// Always 0x0300 for SCI32
        /// </summary>
        public short paletteTag;

        /// <summary>
        /// Length of Palette header + number of colors in palette * 4
        /// </summary>
        public int paletteTotalLength;

        /// <summary>
        /// Initialize struct with default values
        /// </summary>
        /// <param name="bmp"></param>
        public void Initialize(Image bmp)
        {
            paletteTag = 0x0300;
            paletteTotalLength = 1061;
        }
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PalHeader
    {
        /// <summary>
        /// 0x000E is SCI32 0x900E is ?
        /// </summary>
        public short palID;

        /// <summary>
        /// 8 unknown bytes
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] unkBytes1; // first 8 bytes contain palette name ? (Check)
        public byte isSci32; // must be 0x01 to save properly
        public short unkShort1;
        /// <summary>
        /// Size of remaining data (from unkbytes2 to end of palette) 22 + numColors * 4
        /// </summary>
        public short dataLength;

        /// <summary>
        /// 10 unknown bytes
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public byte[] unkBytes2;

        /// <summary>
        /// First color palette starts at valid values 1 - 256
        /// </summary>
        public short firstColor;

        /// <summary>
        /// Unknown 2 bytes
        /// </summary>
        public short unkShort;

        /// <summary>
        /// Total number of colors in the palette
        /// </summary>
        public short numColors;

        /// <summary>
        /// If we use four color pixels or not, for SCI32 we always do
        /// </summary>
        public byte exfourColor;

        /// <summary>
        /// If we use Triple colors for Palette, we do not for SCI32
        /// </summary>
        public byte tripleColor;

        /// <summary>
        /// Unknown 4 bytes
        /// </summary>
        public int unkInt;

        /// <summary>
        /// Initialize struct with default values
        /// </summary>
        /// <param name="bmp"></param>
        public void Initialize(Image bmp)
        {
            palID = 0x0E;
            exfourColor = 1;
            numColors = 256;
            dataLength = 1046;
            isSci32 = 0x01;
        }
    }
    #endregion
    
    #region v56
    [StructLayout(LayoutKind.Sequential, Pack = 1)] // total size 26
    public struct ViewFileHeader
    {
        public int patchId; // 8080
        /// <summary>
        /// 22 unknown bytes
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 22)]
        public byte[] unkBytes1;

        public void Initialize()
        {
            patchId = 0x00008080;

        }
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)] // total size 18
    public struct ViewHeader
    {
        public short loopTableOffset;
        public byte loopCount;
        public byte unkByte1;
        public byte compressed; // always 1 ?
        /// <summary>
        /// 0 = 320x200 1 = 640x480 2 = 640x400
        /// </summary>
        public byte viewSize;
        public short numCells; 
        public int paletteOffset;
        public byte loopRecSize; // always 16
        public byte cellRecSize;
        /// <summary>
        /// if ResX==0 && ResY==0 - look at ViewSize
        /// </summary>
        public short resolutionX;
        /// <summary>
        /// if ResX==0 && ResY==0 - look at ViewSize
        /// </summary>
        public short resolutionY;

        public void Initialize()
        {
            loopTableOffset = 16;
        }
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)] // total size 16
    public struct LoopHeader
    {
        /// <summary>
        /// All the loops in all the cells
        /// </summary>
        public byte numTotalLoops;
        public byte isMirror;
        public byte numCells;
        public int unkInt1;
        public byte unkByte1;
        public int unkInt2;
        public int cellsOffset;
    }
    #endregion


    #region SCR
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ScriptFileHeader
    {
        public short scrTag; // 0x0084
        public int fileLength;
    }
        #endregion


    #region Message
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MessageFileHeader
    {
        public short msgTag; // always 0x008F
        public short sciTag; // Most of the time it is 0x1388 sometimes 0x10F5 1.msg is the latter
        public short unkShort1; // always 0x0000
        public short messagesLength;
        public short unkShort2; // sometimes is the same as phrase count
        public short phraseCount;
    }

    #endregion
}