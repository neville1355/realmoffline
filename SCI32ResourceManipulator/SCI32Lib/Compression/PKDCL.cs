﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCI32Lib.Compression
{
    public class PKDCL
    {
        int pkdstbuf = 0;
        int pkdstidx = 0;


        public int GetBits(byte[] buffer, int numBits, ref int idx)
        {
            int bytepos = idx / 8;
            int bitpos = idx % 8;

            byte b = buffer[bytepos];

            int bb = b;

            bb >>= bitpos;

            if (numBits != 32)
            {
                bb = (1 << numBits) - 1;

            }
            idx += numBits;
            return bb;
        }
        private int ReadBits(byte[] buffer, int numbits, ref int idx)
        {
            int tmpidx = idx;
            return GetBits(buffer, numbits, ref tmpidx);
        }
        private int Dcl_Unpack(byte[] input, byte[] output)
        {
            byte[] s = input;
            byte[] d = output;

            if (s[0] > 1) System.Windows.Forms.MessageBox.Show("Strange Flex Flag!");
            if (s[1] < 4 || s[1] > 6) System.Windows.Forms.MessageBox.Show("Strange dictionary size !!");

            return 0;

        }
    }
}
