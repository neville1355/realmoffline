﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SCI32Lib.Controls
{
    public partial class ImageInformationControl : UserControl
    {
        public ImageInformationControl()
        {
            InitializeComponent();
        }
        public void PopulateData(PictureInformation info)
        {
            // tab 1
            labelSciType.Text = info.P56InfoHeader._sciType.ToString("X2");
            labelCellNum.Text = info.P56InfoHeader._header.NumCells.ToString();
            labelHeaderPicWidth.Text = info.P56InfoHeader._header.Width.ToString();
            labelHeaderPicHeight.Text = info.P56InfoHeader._header.Height.ToString();
            labelCellRecSize.Text = info.P56InfoHeader._header.CellRecSize.ToString("X2");
            labelCellOffset.Text = info.P56InfoHeader._header.CellsOffset.ToString();
            label1HeaderCompression.Text = info.P56InfoHeader._header.IsCompressed.ToString();
            labelHeaderPaletteOffset.Text = info.P56InfoHeader._header.PaletteOffset.ToString();
            //
            labelCellWidth.Text = info.P56CellHeader.Width.ToString();
            labelCellHeight.Text = info.P56CellHeader.Height.ToString();
            labelCellXShift.Text = info.P56CellHeader.XShift.ToString();
            labelCellYShift.Text = info.P56CellHeader.YShift.ToString();
            labelCellTransColor.Text = info.P56CellHeader.TransparentClr.ToString();
            labelCellCompression.Text = info.P56CellHeader.Compression.ToString();
            labelCellPackSize.Text = info.P56CellHeader.ImageandPackSize.ToString();
            labelCellFlags.Text = info.P56CellHeader.Flags.ToString();
            labelCellImageSize.Text = info.P56CellHeader.ImageSize.ToString();
            labelCellPaletteOffset.Text = info.P56CellHeader.PaletteOffs.ToString();
            labelCellImageOffset.Text = info.P56CellHeader.ImageOffs.ToString();
            labelCellPackedOffset.Text = info.P56CellHeader.PackDataOffs.ToString();
            labelCellLinesOffset.Text = info.P56CellHeader.LinesOffs.ToString();
            labelCellZDepth.Text = info.P56CellHeader.ZDepth.ToString();
            labelCellXPos.Text = info.P56CellHeader.XPos.ToString();
            labelCellYPos.Text = info.P56CellHeader.YPos.ToString();
            //
            labelPaletteTag.Text = info.P56PaletteInfoHeader.paletteTag.ToString("X2");
            labelPaletteLength.Text = info.P56PaletteInfoHeader.paletteTotalLength.ToString();
            labelPaletteID.Text = info.P56PaletteHeader.palID.ToString();
            labelPaletteDataLength.Text = info.P56PaletteHeader.dataLength.ToString();
            labelPaletteFirstColor.Text = info.P56PaletteHeader.firstColor.ToString();
            labelPaletteNumColors.Text = info.P56PaletteHeader.numColors.ToString();
            labelPalette4Colors.Text = info.P56PaletteHeader.exfourColor.ToString();
            labelPalette3Colors.Text = info.P56PaletteHeader.tripleColor.ToString();
            //

        }
    }
}
