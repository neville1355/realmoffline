﻿namespace SCI32Lib.Controls
{
    partial class ImageInformationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabP56Header = new System.Windows.Forms.TabPage();
            this.label1HeaderCompression = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelCellOffset = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelCellRecSize = new System.Windows.Forms.Label();
            this.labelHeaderPicHeight = new System.Windows.Forms.Label();
            this.labelHeaderPaletteOffset = new System.Windows.Forms.Label();
            this.labelHeaderPicWidth = new System.Windows.Forms.Label();
            this.labelCellNum = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelSciType = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabCellHeader = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.labelCellYPos = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelCellXPos = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelCellZDepth = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labelCellLinesOffset = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelCellPackedOffset = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.labelCellImageOffset = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelCellPaletteOffset = new System.Windows.Forms.Label();
            this.labelCellFlags = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelCellPackSize = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelCellCompression = new System.Windows.Forms.Label();
            this.labelCellTransColor = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelCellYShift = new System.Windows.Forms.Label();
            this.labelCellXShift = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelCellHeight = new System.Windows.Forms.Label();
            this.labelCellWidth = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelCellImageSize = new System.Windows.Forms.Label();
            this.tabPaletteHeader = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.labelPaletteFirstColor = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.labelPaletteDataLength = new System.Windows.Forms.Label();
            this.labelPaletteID = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.labelPaletteLength = new System.Windows.Forms.Label();
            this.labelPaletteTag = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.labelPalette3Colors = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.labelPalette4Colors = new System.Windows.Forms.Label();
            this.labelPaletteNumColors = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabP56Header.SuspendLayout();
            this.tabCellHeader.SuspendLayout();
            this.tabPaletteHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 242);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Image Information";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabP56Header);
            this.tabControl1.Controls.Add(this.tabCellHeader);
            this.tabControl1.Controls.Add(this.tabPaletteHeader);
            this.tabControl1.Location = new System.Drawing.Point(6, 18);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(271, 215);
            this.tabControl1.TabIndex = 0;
            // 
            // tabP56Header
            // 
            this.tabP56Header.Controls.Add(this.label1HeaderCompression);
            this.tabP56Header.Controls.Add(this.label12);
            this.tabP56Header.Controls.Add(this.labelCellOffset);
            this.tabP56Header.Controls.Add(this.label6);
            this.tabP56Header.Controls.Add(this.label_1);
            this.tabP56Header.Controls.Add(this.label3);
            this.tabP56Header.Controls.Add(this.label8);
            this.tabP56Header.Controls.Add(this.label7);
            this.tabP56Header.Controls.Add(this.labelCellRecSize);
            this.tabP56Header.Controls.Add(this.labelHeaderPicHeight);
            this.tabP56Header.Controls.Add(this.labelHeaderPaletteOffset);
            this.tabP56Header.Controls.Add(this.labelHeaderPicWidth);
            this.tabP56Header.Controls.Add(this.labelCellNum);
            this.tabP56Header.Controls.Add(this.label2);
            this.tabP56Header.Controls.Add(this.labelSciType);
            this.tabP56Header.Controls.Add(this.label1);
            this.tabP56Header.Location = new System.Drawing.Point(4, 22);
            this.tabP56Header.Name = "tabP56Header";
            this.tabP56Header.Padding = new System.Windows.Forms.Padding(3);
            this.tabP56Header.Size = new System.Drawing.Size(237, 189);
            this.tabP56Header.TabIndex = 0;
            this.tabP56Header.Text = "P56 Header Info";
            this.tabP56Header.UseVisualStyleBackColor = true;
            // 
            // label1HeaderCompression
            // 
            this.label1HeaderCompression.AutoSize = true;
            this.label1HeaderCompression.Location = new System.Drawing.Point(93, 159);
            this.label1HeaderCompression.Name = "label1HeaderCompression";
            this.label1HeaderCompression.Size = new System.Drawing.Size(13, 13);
            this.label1HeaderCompression.TabIndex = 17;
            this.label1HeaderCompression.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 159);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Is Compressed:";
            // 
            // labelCellOffset
            // 
            this.labelCellOffset.AutoSize = true;
            this.labelCellOffset.Location = new System.Drawing.Point(93, 134);
            this.labelCellOffset.Name = "labelCellOffset";
            this.labelCellOffset.Size = new System.Drawing.Size(13, 13);
            this.labelCellOffset.TabIndex = 13;
            this.labelCellOffset.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Cell Offset:";
            // 
            // label_1
            // 
            this.label_1.AutoSize = true;
            this.label_1.Location = new System.Drawing.Point(127, 14);
            this.label_1.Name = "label_1";
            this.label_1.Size = new System.Drawing.Size(74, 13);
            this.label_1.TabIndex = 11;
            this.label_1.Text = "Palette Offset:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Cell Rec Size:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Picture Height:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Picture Width:";
            // 
            // labelCellRecSize
            // 
            this.labelCellRecSize.AutoSize = true;
            this.labelCellRecSize.Location = new System.Drawing.Point(93, 108);
            this.labelCellRecSize.Name = "labelCellRecSize";
            this.labelCellRecSize.Size = new System.Drawing.Size(13, 13);
            this.labelCellRecSize.TabIndex = 7;
            this.labelCellRecSize.Text = "0";
            // 
            // labelHeaderPicHeight
            // 
            this.labelHeaderPicHeight.AutoSize = true;
            this.labelHeaderPicHeight.Location = new System.Drawing.Point(93, 84);
            this.labelHeaderPicHeight.Name = "labelHeaderPicHeight";
            this.labelHeaderPicHeight.Size = new System.Drawing.Size(13, 13);
            this.labelHeaderPicHeight.TabIndex = 6;
            this.labelHeaderPicHeight.Text = "0";
            // 
            // labelHeaderPaletteOffset
            // 
            this.labelHeaderPaletteOffset.AutoSize = true;
            this.labelHeaderPaletteOffset.Location = new System.Drawing.Point(218, 14);
            this.labelHeaderPaletteOffset.Name = "labelHeaderPaletteOffset";
            this.labelHeaderPaletteOffset.Size = new System.Drawing.Size(13, 13);
            this.labelHeaderPaletteOffset.TabIndex = 5;
            this.labelHeaderPaletteOffset.Text = "0";
            // 
            // labelHeaderPicWidth
            // 
            this.labelHeaderPicWidth.AutoSize = true;
            this.labelHeaderPicWidth.Location = new System.Drawing.Point(93, 61);
            this.labelHeaderPicWidth.Name = "labelHeaderPicWidth";
            this.labelHeaderPicWidth.Size = new System.Drawing.Size(13, 13);
            this.labelHeaderPicWidth.TabIndex = 4;
            this.labelHeaderPicWidth.Text = "0";
            // 
            // labelCellNum
            // 
            this.labelCellNum.AutoSize = true;
            this.labelCellNum.Location = new System.Drawing.Point(93, 37);
            this.labelCellNum.Name = "labelCellNum";
            this.labelCellNum.Size = new System.Drawing.Size(13, 13);
            this.labelCellNum.TabIndex = 3;
            this.labelCellNum.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cells:";
            // 
            // labelSciType
            // 
            this.labelSciType.AutoSize = true;
            this.labelSciType.Location = new System.Drawing.Point(93, 14);
            this.labelSciType.Name = "labelSciType";
            this.labelSciType.Size = new System.Drawing.Size(13, 13);
            this.labelSciType.TabIndex = 1;
            this.labelSciType.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SCI Type:";
            // 
            // tabCellHeader
            // 
            this.tabCellHeader.Controls.Add(this.label22);
            this.tabCellHeader.Controls.Add(this.labelCellYPos);
            this.tabCellHeader.Controls.Add(this.label25);
            this.tabCellHeader.Controls.Add(this.labelCellXPos);
            this.tabCellHeader.Controls.Add(this.label20);
            this.tabCellHeader.Controls.Add(this.labelCellZDepth);
            this.tabCellHeader.Controls.Add(this.label23);
            this.tabCellHeader.Controls.Add(this.labelCellLinesOffset);
            this.tabCellHeader.Controls.Add(this.label9);
            this.tabCellHeader.Controls.Add(this.labelCellPackedOffset);
            this.tabCellHeader.Controls.Add(this.label21);
            this.tabCellHeader.Controls.Add(this.labelCellImageOffset);
            this.tabCellHeader.Controls.Add(this.label18);
            this.tabCellHeader.Controls.Add(this.labelCellPaletteOffset);
            this.tabCellHeader.Controls.Add(this.labelCellFlags);
            this.tabCellHeader.Controls.Add(this.label19);
            this.tabCellHeader.Controls.Add(this.labelCellPackSize);
            this.tabCellHeader.Controls.Add(this.label16);
            this.tabCellHeader.Controls.Add(this.label11);
            this.tabCellHeader.Controls.Add(this.label13);
            this.tabCellHeader.Controls.Add(this.labelCellCompression);
            this.tabCellHeader.Controls.Add(this.labelCellTransColor);
            this.tabCellHeader.Controls.Add(this.label14);
            this.tabCellHeader.Controls.Add(this.label15);
            this.tabCellHeader.Controls.Add(this.labelCellYShift);
            this.tabCellHeader.Controls.Add(this.labelCellXShift);
            this.tabCellHeader.Controls.Add(this.label4);
            this.tabCellHeader.Controls.Add(this.label10);
            this.tabCellHeader.Controls.Add(this.labelCellHeight);
            this.tabCellHeader.Controls.Add(this.labelCellWidth);
            this.tabCellHeader.Controls.Add(this.label5);
            this.tabCellHeader.Controls.Add(this.labelCellImageSize);
            this.tabCellHeader.Location = new System.Drawing.Point(4, 22);
            this.tabCellHeader.Name = "tabCellHeader";
            this.tabCellHeader.Padding = new System.Windows.Forms.Padding(3);
            this.tabCellHeader.Size = new System.Drawing.Size(263, 189);
            this.tabCellHeader.TabIndex = 1;
            this.tabCellHeader.Text = "P56 Cell Header";
            this.tabCellHeader.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(127, 170);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 13);
            this.label22.TabIndex = 45;
            this.label22.Text = "Image YPos:";
            // 
            // labelCellYPos
            // 
            this.labelCellYPos.AutoSize = true;
            this.labelCellYPos.Location = new System.Drawing.Point(218, 170);
            this.labelCellYPos.Name = "labelCellYPos";
            this.labelCellYPos.Size = new System.Drawing.Size(13, 13);
            this.labelCellYPos.TabIndex = 44;
            this.labelCellYPos.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(147, 147);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 13);
            this.label25.TabIndex = 43;
            this.label25.Text = "Image XPos:";
            // 
            // labelCellXPos
            // 
            this.labelCellXPos.AutoSize = true;
            this.labelCellXPos.Location = new System.Drawing.Point(238, 147);
            this.labelCellXPos.Name = "labelCellXPos";
            this.labelCellXPos.Size = new System.Drawing.Size(13, 13);
            this.labelCellXPos.TabIndex = 42;
            this.labelCellXPos.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(127, 125);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "Z Depth:";
            // 
            // labelCellZDepth
            // 
            this.labelCellZDepth.AutoSize = true;
            this.labelCellZDepth.Location = new System.Drawing.Point(218, 125);
            this.labelCellZDepth.Name = "labelCellZDepth";
            this.labelCellZDepth.Size = new System.Drawing.Size(13, 13);
            this.labelCellZDepth.TabIndex = 40;
            this.labelCellZDepth.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(127, 102);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(66, 13);
            this.label23.TabIndex = 39;
            this.label23.Text = "Lines Offset:";
            // 
            // labelCellLinesOffset
            // 
            this.labelCellLinesOffset.AutoSize = true;
            this.labelCellLinesOffset.Location = new System.Drawing.Point(218, 102);
            this.labelCellLinesOffset.Name = "labelCellLinesOffset";
            this.labelCellLinesOffset.Size = new System.Drawing.Size(13, 13);
            this.labelCellLinesOffset.TabIndex = 38;
            this.labelCellLinesOffset.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(127, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 37;
            this.label9.Text = "Packed Offset:";
            // 
            // labelCellPackedOffset
            // 
            this.labelCellPackedOffset.AutoSize = true;
            this.labelCellPackedOffset.Location = new System.Drawing.Point(218, 80);
            this.labelCellPackedOffset.Name = "labelCellPackedOffset";
            this.labelCellPackedOffset.Size = new System.Drawing.Size(13, 13);
            this.labelCellPackedOffset.TabIndex = 36;
            this.labelCellPackedOffset.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(127, 57);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(70, 13);
            this.label21.TabIndex = 35;
            this.label21.Text = "Image Offset:";
            // 
            // labelCellImageOffset
            // 
            this.labelCellImageOffset.AutoSize = true;
            this.labelCellImageOffset.Location = new System.Drawing.Point(218, 57);
            this.labelCellImageOffset.Name = "labelCellImageOffset";
            this.labelCellImageOffset.Size = new System.Drawing.Size(13, 13);
            this.labelCellImageOffset.TabIndex = 34;
            this.labelCellImageOffset.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(127, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Palette Offset:";
            // 
            // labelCellPaletteOffset
            // 
            this.labelCellPaletteOffset.AutoSize = true;
            this.labelCellPaletteOffset.Location = new System.Drawing.Point(218, 35);
            this.labelCellPaletteOffset.Name = "labelCellPaletteOffset";
            this.labelCellPaletteOffset.Size = new System.Drawing.Size(13, 13);
            this.labelCellPaletteOffset.TabIndex = 32;
            this.labelCellPaletteOffset.Text = "0";
            // 
            // labelCellFlags
            // 
            this.labelCellFlags.AutoSize = true;
            this.labelCellFlags.Location = new System.Drawing.Point(93, 169);
            this.labelCellFlags.Name = "labelCellFlags";
            this.labelCellFlags.Size = new System.Drawing.Size(13, 13);
            this.labelCellFlags.TabIndex = 31;
            this.labelCellFlags.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 169);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "Flags:";
            // 
            // labelCellPackSize
            // 
            this.labelCellPackSize.AutoSize = true;
            this.labelCellPackSize.Location = new System.Drawing.Point(82, 147);
            this.labelCellPackSize.Name = "labelCellPackSize";
            this.labelCellPackSize.Size = new System.Drawing.Size(13, 13);
            this.labelCellPackSize.TabIndex = 29;
            this.labelCellPackSize.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 147);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 13);
            this.label16.TabIndex = 28;
            this.label16.Text = "Packed Size:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "Compression:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 102);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Trans Color:";
            // 
            // labelCellCompression
            // 
            this.labelCellCompression.AutoSize = true;
            this.labelCellCompression.Location = new System.Drawing.Point(93, 125);
            this.labelCellCompression.Name = "labelCellCompression";
            this.labelCellCompression.Size = new System.Drawing.Size(13, 13);
            this.labelCellCompression.TabIndex = 25;
            this.labelCellCompression.Text = "0";
            // 
            // labelCellTransColor
            // 
            this.labelCellTransColor.AutoSize = true;
            this.labelCellTransColor.Location = new System.Drawing.Point(93, 102);
            this.labelCellTransColor.Name = "labelCellTransColor";
            this.labelCellTransColor.Size = new System.Drawing.Size(13, 13);
            this.labelCellTransColor.TabIndex = 24;
            this.labelCellTransColor.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Picture Y Shift:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Picture X Shift:";
            // 
            // labelCellYShift
            // 
            this.labelCellYShift.AutoSize = true;
            this.labelCellYShift.Location = new System.Drawing.Point(93, 80);
            this.labelCellYShift.Name = "labelCellYShift";
            this.labelCellYShift.Size = new System.Drawing.Size(13, 13);
            this.labelCellYShift.TabIndex = 21;
            this.labelCellYShift.Text = "0";
            // 
            // labelCellXShift
            // 
            this.labelCellXShift.AutoSize = true;
            this.labelCellXShift.Location = new System.Drawing.Point(93, 57);
            this.labelCellXShift.Name = "labelCellXShift";
            this.labelCellXShift.Size = new System.Drawing.Size(13, 13);
            this.labelCellXShift.TabIndex = 20;
            this.labelCellXShift.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Picture Height:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 12);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Picture Width:";
            // 
            // labelCellHeight
            // 
            this.labelCellHeight.AutoSize = true;
            this.labelCellHeight.Location = new System.Drawing.Point(93, 35);
            this.labelCellHeight.Name = "labelCellHeight";
            this.labelCellHeight.Size = new System.Drawing.Size(13, 13);
            this.labelCellHeight.TabIndex = 17;
            this.labelCellHeight.Text = "0";
            // 
            // labelCellWidth
            // 
            this.labelCellWidth.AutoSize = true;
            this.labelCellWidth.Location = new System.Drawing.Point(93, 12);
            this.labelCellWidth.Name = "labelCellWidth";
            this.labelCellWidth.Size = new System.Drawing.Size(13, 13);
            this.labelCellWidth.TabIndex = 16;
            this.labelCellWidth.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(127, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Image Size:";
            // 
            // labelCellImageSize
            // 
            this.labelCellImageSize.AutoSize = true;
            this.labelCellImageSize.Location = new System.Drawing.Point(218, 12);
            this.labelCellImageSize.Name = "labelCellImageSize";
            this.labelCellImageSize.Size = new System.Drawing.Size(13, 13);
            this.labelCellImageSize.TabIndex = 14;
            this.labelCellImageSize.Text = "0";
            // 
            // tabPaletteHeader
            // 
            this.tabPaletteHeader.Controls.Add(this.label34);
            this.tabPaletteHeader.Controls.Add(this.labelPalette3Colors);
            this.tabPaletteHeader.Controls.Add(this.label36);
            this.tabPaletteHeader.Controls.Add(this.label37);
            this.tabPaletteHeader.Controls.Add(this.labelPalette4Colors);
            this.tabPaletteHeader.Controls.Add(this.labelPaletteNumColors);
            this.tabPaletteHeader.Controls.Add(this.label17);
            this.tabPaletteHeader.Controls.Add(this.labelPaletteFirstColor);
            this.tabPaletteHeader.Controls.Add(this.label26);
            this.tabPaletteHeader.Controls.Add(this.label27);
            this.tabPaletteHeader.Controls.Add(this.labelPaletteDataLength);
            this.tabPaletteHeader.Controls.Add(this.labelPaletteID);
            this.tabPaletteHeader.Controls.Add(this.label30);
            this.tabPaletteHeader.Controls.Add(this.label31);
            this.tabPaletteHeader.Controls.Add(this.labelPaletteLength);
            this.tabPaletteHeader.Controls.Add(this.labelPaletteTag);
            this.tabPaletteHeader.Location = new System.Drawing.Point(4, 22);
            this.tabPaletteHeader.Name = "tabPaletteHeader";
            this.tabPaletteHeader.Padding = new System.Windows.Forms.Padding(3);
            this.tabPaletteHeader.Size = new System.Drawing.Size(263, 189);
            this.tabPaletteHeader.TabIndex = 2;
            this.tabPaletteHeader.Text = "Palette Header";
            this.tabPaletteHeader.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 102);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 36;
            this.label17.Text = "First Color:";
            // 
            // labelPaletteFirstColor
            // 
            this.labelPaletteFirstColor.AutoSize = true;
            this.labelPaletteFirstColor.Location = new System.Drawing.Point(93, 102);
            this.labelPaletteFirstColor.Name = "labelPaletteFirstColor";
            this.labelPaletteFirstColor.Size = new System.Drawing.Size(13, 13);
            this.labelPaletteFirstColor.TabIndex = 35;
            this.labelPaletteFirstColor.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 80);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(69, 13);
            this.label26.TabIndex = 34;
            this.label26.Text = "Data Length:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 57);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(57, 13);
            this.label27.TabIndex = 33;
            this.label27.Text = "Palette ID:";
            // 
            // labelPaletteDataLength
            // 
            this.labelPaletteDataLength.AutoSize = true;
            this.labelPaletteDataLength.Location = new System.Drawing.Point(93, 80);
            this.labelPaletteDataLength.Name = "labelPaletteDataLength";
            this.labelPaletteDataLength.Size = new System.Drawing.Size(13, 13);
            this.labelPaletteDataLength.TabIndex = 32;
            this.labelPaletteDataLength.Text = "0";
            // 
            // labelPaletteID
            // 
            this.labelPaletteID.AutoSize = true;
            this.labelPaletteID.Location = new System.Drawing.Point(93, 57);
            this.labelPaletteID.Name = "labelPaletteID";
            this.labelPaletteID.Size = new System.Drawing.Size(13, 13);
            this.labelPaletteID.TabIndex = 31;
            this.labelPaletteID.Text = "0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 35);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(79, 13);
            this.label30.TabIndex = 30;
            this.label30.Text = "Palette Length:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 12);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(65, 13);
            this.label31.TabIndex = 29;
            this.label31.Text = "Palette Tag:";
            // 
            // labelPaletteLength
            // 
            this.labelPaletteLength.AutoSize = true;
            this.labelPaletteLength.Location = new System.Drawing.Point(93, 35);
            this.labelPaletteLength.Name = "labelPaletteLength";
            this.labelPaletteLength.Size = new System.Drawing.Size(13, 13);
            this.labelPaletteLength.TabIndex = 28;
            this.labelPaletteLength.Text = "0";
            // 
            // labelPaletteTag
            // 
            this.labelPaletteTag.AutoSize = true;
            this.labelPaletteTag.Location = new System.Drawing.Point(93, 12);
            this.labelPaletteTag.Name = "labelPaletteTag";
            this.labelPaletteTag.Size = new System.Drawing.Size(13, 13);
            this.labelPaletteTag.TabIndex = 27;
            this.labelPaletteTag.Text = "0";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 169);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 13);
            this.label34.TabIndex = 42;
            this.label34.Text = "Is 3 Colors:";
            // 
            // labelPalette3Colors
            // 
            this.labelPalette3Colors.AutoSize = true;
            this.labelPalette3Colors.Location = new System.Drawing.Point(93, 169);
            this.labelPalette3Colors.Name = "labelPalette3Colors";
            this.labelPalette3Colors.Size = new System.Drawing.Size(13, 13);
            this.labelPalette3Colors.TabIndex = 41;
            this.labelPalette3Colors.Text = "0";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 147);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(59, 13);
            this.label36.TabIndex = 40;
            this.label36.Text = "Is 4 Colors:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 124);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(64, 13);
            this.label37.TabIndex = 39;
            this.label37.Text = "Num Colors:";
            // 
            // labelPalette4Colors
            // 
            this.labelPalette4Colors.AutoSize = true;
            this.labelPalette4Colors.Location = new System.Drawing.Point(93, 147);
            this.labelPalette4Colors.Name = "labelPalette4Colors";
            this.labelPalette4Colors.Size = new System.Drawing.Size(13, 13);
            this.labelPalette4Colors.TabIndex = 38;
            this.labelPalette4Colors.Text = "0";
            // 
            // labelPaletteNumColors
            // 
            this.labelPaletteNumColors.AutoSize = true;
            this.labelPaletteNumColors.Location = new System.Drawing.Point(93, 124);
            this.labelPaletteNumColors.Name = "labelPaletteNumColors";
            this.labelPaletteNumColors.Size = new System.Drawing.Size(13, 13);
            this.labelPaletteNumColors.TabIndex = 37;
            this.labelPaletteNumColors.Text = "0";
            // 
            // ImageInformationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "ImageInformationControl";
            this.Size = new System.Drawing.Size(289, 249);
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabP56Header.ResumeLayout(false);
            this.tabP56Header.PerformLayout();
            this.tabCellHeader.ResumeLayout(false);
            this.tabCellHeader.PerformLayout();
            this.tabPaletteHeader.ResumeLayout(false);
            this.tabPaletteHeader.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabP56Header;
        private System.Windows.Forms.Label label1HeaderCompression;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelCellOffset;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelCellRecSize;
        private System.Windows.Forms.Label labelHeaderPicHeight;
        private System.Windows.Forms.Label labelHeaderPaletteOffset;
        private System.Windows.Forms.Label labelHeaderPicWidth;
        private System.Windows.Forms.Label labelCellNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabCellHeader;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label labelCellYPos;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label labelCellXPos;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label labelCellZDepth;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label labelCellLinesOffset;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelCellPackedOffset;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label labelCellImageOffset;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labelCellPaletteOffset;
        private System.Windows.Forms.Label labelCellFlags;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labelCellPackSize;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelCellCompression;
        private System.Windows.Forms.Label labelCellTransColor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelCellYShift;
        private System.Windows.Forms.Label labelCellXShift;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelCellHeight;
        private System.Windows.Forms.Label labelCellWidth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelCellImageSize;
        private System.Windows.Forms.TabPage tabPaletteHeader;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelPaletteFirstColor;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label labelPaletteDataLength;
        private System.Windows.Forms.Label labelPaletteID;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label labelPaletteLength;
        private System.Windows.Forms.Label labelPaletteTag;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label labelPalette3Colors;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label labelPalette4Colors;
        private System.Windows.Forms.Label labelPaletteNumColors;
        private System.Windows.Forms.Label labelSciType;
    }
}
