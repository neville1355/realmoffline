﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCI32Lib.Classes
{
    public class SCI32Loop
    {
        private List<SCI32Cell> _cells;
        public List<SCI32Cell> Cells
        {
            get { return _cells; }
        }
        private byte _totalLoops;
        public byte TotalLoops
        {
            get { return _totalLoops; }
        }
        private bool _isMirror;
        public bool IsMirror
        {
            get { return _isMirror; }
        }
        private int _cellsOffset;
        public int CellsOffset
        {
            get { return _cellsOffset; }
        }
       
        int unkInt1;
        byte unkByte1;
        int unkInt2;
        

        public SCI32Loop(LoopHeader _header)
        {
            _cells = new List<SCI32Cell>();
            _totalLoops = _header.numTotalLoops;
            _isMirror = Convert.ToBoolean(_header.isMirror);
            _cellsOffset = _header.cellsOffset;
            unkInt1 = _header.unkInt1;
            unkByte1 = _header.unkByte1;
            unkInt2 = _header.unkInt2;
        }
        public SCI32Loop(LoopHeader _header, SCI32Cell[] cells)
        {
            _cells = new List<SCI32Cell>();
            _cells.AddRange(cells);
            _totalLoops = _header.numTotalLoops;
            _isMirror = Convert.ToBoolean(_header.isMirror);
            _cellsOffset = _header.cellsOffset;
            unkInt1 = _header.unkInt1;
            unkByte1 = _header.unkByte1;
            unkInt2 = _header.unkInt2;
        }
        public SCI32Cell GetCell(int id)
        {
            return _cells[id];
        }

        public void AddCell(SCI32Cell cell)
        {
            _cells.Add(cell);
        }
        public LoopHeader RestoreLoopHeader()
        {
            LoopHeader result = new LoopHeader();
            result.cellsOffset = _cellsOffset;
            result.isMirror = BitConverter.GetBytes(_isMirror)[0];
            result.numCells = (byte)_cells.Count;
            result.numTotalLoops = _totalLoops;
            result.unkByte1 = unkByte1;
            result.unkInt1 = unkInt1;
            result.unkInt2 = unkInt2;
            return result;
        }
    }
}
