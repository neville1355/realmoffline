﻿using SCI32Lib.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SCI32Lib
{
    public class SCI32Cell
    {
        private short imageTag = 0x0400;
        public short ImageTag
        {
            get { return imageTag; }
        }
        private Bitmap image;
        public Bitmap Image
        {
            get { return image; }
        }
        public SCI32Cell(Bitmap bmp)
        {
            SetImage(bmp);
        }
        private Hashtable m_knownColors = new Hashtable((int)Math.Pow(2, 20), 1.0f);


        public SCI32Cell()
        {

        }
        public void SetImage(Bitmap bmp)
        {
            /*
            Bitmap newBm = bmp.Clone(new Rectangle(0, 0, bmp.Width, bmp.Height), PixelFormat.Format8bppIndexed);
            if(bmp.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                Rectangle cloneRect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                System.Drawing.Imaging.PixelFormat format =
                    PixelFormat.Format8bppIndexed;
                Bitmap cloneBitmap = bmp.Clone(cloneRect, format);

                // Draw the cloned portion of the Bitmap object.
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.DrawImage(cloneBitmap, 0, 0);
                }
                bmp = cloneBitmap;
            }
            this.image = bmp;
            */
            //
           // Bitmap newBm = ChangePixelFormat(bmp, PixelFormat.Format8bppIndexed);
            if (bmp.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                Bitmap a = InternalTools.ConvertImageTo256Colors(bmp);
                System.Windows.Forms.MessageBox.Show(string.Format("{0} COlors ", a.Palette.Entries.Length));
               // b.Palette = p;

                this.image = a;

                // Image newImg = null;
                /*
                using (var ms = new MemoryStream())
                {
                    bmp.Save(ms, ImageFormat.Gif);
                    ms.Position = 0;
                    //newImg = System.Drawing.Image.FromStream(ms, true);
                    this.image = new Bitmap(System.Drawing.Bitmap.FromStream(ms, false));
                }
                */
                
                //System.Windows.Forms.MessageBox.Show(string.Format("Image set in cell it has {0} colors and is format {1}.", newImg.Palette.Entries.Length, newImg.PixelFormat));
            }
            else this.image = bmp;

            
        }
        private byte[] sciFile;
        public byte[] SCIFile
        {
            get { return sciFile; }
            set { sciFile = value; }
        }
      

        public static bool CheckCompatibility(Bitmap bmp1, Bitmap bmp2)
        {
            return ((bmp1.Width == bmp2.Width) && (bmp1.Height == bmp2.Height) && (bmp1.PixelFormat == bmp2.PixelFormat));
        }
        public byte[] ReadCell(BinaryReader reader, int imgWidth, int imgHeight)
        {
            imageTag = reader.ReadInt16();
           int length = reader.ReadInt32();
            return reader.ReadBytes(imgWidth * imgHeight);
        }
        public byte[] WriteCell(BinaryWriter writer)
        {
            // Bitmap must be 8pp !!!!
            if (Image == null) throw new Exception("CellImage is NULL !!");
            if (Image.PixelFormat != System.Drawing.Imaging.PixelFormat.Format8bppIndexed) throw new Exception(string.Format("Image format is {0}, it must be {1}!!", Image.PixelFormat, PixelFormat.Format8bppIndexed));

            writer.Write(ImageTag);

            //writer.Write(0x00);

            byte[] img = InternalTools.BitmapToByteArray(Image);

            //if (img.Length != Image.Width * Image.Height) throw new Exception(string.Format("Incorrect byte array size, should be {0} but is really {1}", Image.Width * Image.Height, img.Length));

            // we have passed all req's
            writer.Write(img);

            return img;
        }
        private static Bitmap ConvertTo8bpp(Bitmap bmpIn)
        {
            Bitmap bmpOut = new Bitmap(bmpIn.Width, bmpIn.Height, PixelFormat.Format8bppIndexed);

            Graphics grPhoto = Graphics.FromImage(bmpIn);
            grPhoto.DrawImage(bmpOut, new Rectangle(0, 0, bmpOut.Width, bmpOut.Height), 0, 0, bmpOut.Width, bmpOut.Height, GraphicsUnit.Pixel);
            return bmpOut;
        }
    }
}

