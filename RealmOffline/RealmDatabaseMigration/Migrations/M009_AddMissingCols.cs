﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmDatabaseMigration.Migrations {
    [Migration(9)]
    public class M009_AddMissingCols: Migration {
        public override void Up() {
            Alter.Table("BCharacter")
                .AddColumn("Race").AsInt32().Nullable();

            Alter.Table("WorldObject")
                .AddColumn("Height").AsInt32().Nullable()
                .AddColumn("Girth").AsInt32().Nullable();
        }

        public override void Down() {
            Delete.Column("Height").FromTable("WorldObject");
            Delete.Column("Girth").FromTable("WorldObject");
            Delete.Column("Race").FromTable("BCharacter");
        }
    }
}
