﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmDatabaseMigration.Migrations {
    [Migration(5)]
    public class M005_CreateBWearableTable: Migration {
        public override void Up() {
            Create.Table("BWearable")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("OwnerId").AsInt32().ForeignKey("WorldObject", "Id").Nullable()
                .WithColumn("AreaWorn").AsInt32().Nullable()
                .WithColumn("Mask").AsInt32().Nullable()
                .WithColumn("Layer").AsInt32().Nullable()
                .WithColumn("MinimumLevel").AsInt32().Nullable();
        }

        public override void Down() {
            Delete.ForeignKey().FromTable("BWearable").ForeignColumn("OwnerId").ToTable("WorldObject").PrimaryColumn("Id");
            Delete.Table("BWearable");
        }
    }
}
