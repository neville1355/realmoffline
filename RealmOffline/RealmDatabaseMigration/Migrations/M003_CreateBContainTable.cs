﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmDatabaseMigration.Migrations {
    [Migration(3)]
    public class M003_CreateBContainTable: Migration {
        public override void Up() {
            Create.Table("BContain")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("OwnerId").AsInt32().ForeignKey("WorldObject", "Id").Nullable()
                .WithColumn("WeightCapacity").AsInt32().Nullable()
                .WithColumn("BulkCapacity").AsInt32().Nullable()
                .WithColumn("WeightReduction").AsInt32().Nullable()
                .WithColumn("BulkReduction").AsInt32().Nullable();
        }

        public override void Down() {
            Delete.ForeignKey().FromTable("BContain").ForeignColumn("OwnerId").ToTable("WorldObject").PrimaryColumn("Id");
            Delete.Table("BContain");
        }
    }
}
