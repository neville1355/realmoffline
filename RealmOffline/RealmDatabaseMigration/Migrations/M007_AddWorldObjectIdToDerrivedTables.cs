﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmDatabaseMigration.Migrations {
    [Migration(7)]
    public class M007_AddWorldObjectIdToDerrivedTables: Migration {
        public override void Up() {
            Alter.Table("BCharacter")
                .AddColumn("WorldObjectId").AsInt32().ForeignKey("WorldObject", "Id").Nullable();

            Alter.Table("BContain")
                .AddColumn("WorldObjectId").AsInt32().ForeignKey("WorldObject", "Id").Nullable();

            Alter.Table("BHead")
                .AddColumn("WorldObjectId").AsInt32().ForeignKey("WorldObject", "Id").Nullable();

            Alter.Table("BWearable")
                .AddColumn("WorldObjectId").AsInt32().ForeignKey("WorldObject", "Id").Nullable();

            Alter.Table("BWeapon")
                .AddColumn("WorldObjectId").AsInt32().ForeignKey("WorldObject", "Id").Nullable();
        }

        public override void Down() {
            Delete.ForeignKey().FromTable("BCharacter").ForeignColumn("WorldObjectId").ToTable("WorldObject").PrimaryColumn("Id");
            Delete.ForeignKey().FromTable("BContain").ForeignColumn("WorldObjectId").ToTable("WorldObject").PrimaryColumn("Id");
            Delete.ForeignKey().FromTable("BHead").ForeignColumn("WorldObjectId").ToTable("WorldObject").PrimaryColumn("Id");
            Delete.ForeignKey().FromTable("BWeapon").ForeignColumn("WorldObjectId").ToTable("WorldObject").PrimaryColumn("Id");
            Delete.ForeignKey().FromTable("BWearable").ForeignColumn("WorldObjectId").ToTable("WorldObject").PrimaryColumn("Id");
            
            Delete.Column("WorldObjectId").FromTable("BCharacter");
            Delete.Column("WorldObjectId").FromTable("BContain");
            Delete.Column("WorldObjectId").FromTable("BHead");
            Delete.Column("WorldObjectId").FromTable("BWearable");
            Delete.Column("WorldObjectId").FromTable("BWeapon");
        }
    }
}
