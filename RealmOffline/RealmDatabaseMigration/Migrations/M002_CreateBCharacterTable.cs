﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmDatabaseMigration.Migrations {
    [Migration(2)]
    public class M002_CreateBCharacterTable: Migration {
        public override void Up() {
            Create.Table("BCharacter")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("OwnerId").AsInt32().ForeignKey("WorldObject", "Id").Nullable()
                .WithColumn("Name").AsString().Nullable()
                .WithColumn("Title").AsString().Nullable()
                .WithColumn("Profession").AsInt32().Nullable()
                .WithColumn("Experience").AsInt32().Nullable()
                .WithColumn("Sex").AsInt32().Nullable()
                .WithColumn("BuildPoints").AsInt32().Nullable()
                .WithColumn("HomeDown").AsInt32().Nullable()
                .WithColumn("StealingCount").AsInt32().Nullable()
                .WithColumn("KillingCount").AsInt32().Nullable()
                .WithColumn("StealingUnserved").AsInt32().Nullable()
                .WithColumn("KillingUnserved").AsInt32().Nullable()
                .WithColumn("Peaceful").AsInt32().Nullable()
                .WithColumn("Version").AsInt32().Nullable()
                .WithColumn("QuestNumber").AsInt32().Nullable()
                .WithColumn("QuestState").AsInt32().Nullable()
                .WithColumn("WarnCount").AsInt32().Nullable()
                .WithColumn("PlayerKills").AsInt32().Nullable()
                .WithColumn("NpcKills").AsInt32().Nullable()
                .WithColumn("AttackSkill").AsInt32().Nullable()
                .WithColumn("DodgeSkill").AsInt32().Nullable()
                .WithColumn("NumAttacks").AsInt32().Nullable()
                .WithColumn("NumDodges").AsInt32().Nullable()
                .WithColumn("NumBlocks").AsInt32().Nullable()
                .WithColumn("NumMoves").AsInt32().Nullable()
                .WithColumn("CanRetaliate").AsInt32().Nullable()
                .WithColumn("CombatRound").AsInt32().Nullable()
                .WithColumn("SoundGroup").AsInt32().Nullable();
        }

        public override void Down() {
            Delete.ForeignKey().FromTable("BCharacter").ForeignColumn("OwnerId").ToTable("WorldObject").PrimaryColumn("Id");
            Delete.Table("BCharacter");
        }
    }
}
