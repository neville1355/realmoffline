﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmDatabaseMigration.Migrations {
    [Migration(6)]
    public class M006_CreateBWeaponTable: Migration {
        public override void Up() {
            Create.Table("BWeapon")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("OwnerId").AsInt32().ForeignKey("WorldObject", "Id").Nullable()
                .WithColumn("DamageType").AsInt32().Nullable()
                .WithColumn("Speed").AsInt32().Nullable()
                .WithColumn("MinDamage").AsInt32().Nullable()
                .WithColumn("MaxDamage").AsInt32().Nullable()
                .WithColumn("Modifier").AsInt32().Nullable()
                .WithColumn("Pierce").AsInt32().Nullable()
                .WithColumn("Hands").AsInt32().Nullable()
                .WithColumn("Bonus").AsInt32().Nullable()
                .WithColumn("Distance").AsInt32().Nullable()
                .WithColumn("IsMissile").AsInt32().Nullable()
                .WithColumn("SkillType").AsInt32().Nullable();
        }

        public override void Down() {
            Delete.ForeignKey().FromTable("BWeapon").ForeignColumn("OwnerId").ToTable("WorldObject").PrimaryColumn("Id");
            Delete.Table("BWeapon");
        }
    }
}
