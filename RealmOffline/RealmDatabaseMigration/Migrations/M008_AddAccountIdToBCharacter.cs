﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmDatabaseMigration.Migrations {
    [Migration(8)]
    public class M008_AddAccountIdToBCharacter: Migration {
        public override void Up() {
            //this should be a FK but I'm not sure which table... hmm
            Alter.Table("BCharacter")
                .AddColumn("AccountId")
                .AsInt32();
        }

        public override void Down() {
            Delete.Column("AccountId").FromTable("BCharacter");
        }
    }
}
