﻿using RealmOffline.Managers;
using RealmOffline.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOfflineRouter.Requests {
    class GameListRequest: BaseRouterRequest {
        public GameListRequest(byte[] packets) : base(packets) { }

        public override byte[] GetResponse() {
            List<ServerObject> gameServers = GetGameServers();

            WriteHeader();            

            PacketWriter.WriteByte((byte)gameServers.Count);    // Number of servers
            foreach (ServerObject ob in gameServers) {
                PacketWriter.WriteString(ob.Name);
                PacketWriter.WriteString(ob.Up);
                PacketWriter.WriteString(ob.LoggedOnPlayerCount);
                PacketWriter.WriteString(ob.Address);
                PacketWriter.WriteString(ob.Port.ToString());
            }
            PacketWriter.WriteInt32(0);
            PacketWriter.WriteInt32(0);

            return PacketWriter.ToArray();
        }

        private List<ServerObject> GetGameServers() {
            List<ServerObject> s = XMLManager.GetGameServers();

            foreach (ServerObject sv in s) {
                // Give me time to get a update
                sv.GetServerData();
                System.Threading.Thread.Sleep(100);
            }

            return s;
        }

    }
}
