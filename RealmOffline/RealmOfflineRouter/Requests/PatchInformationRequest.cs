﻿using RealmOffline.Base;
using RealmOffline.Managers;
using RealmOffline.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOfflineRouter.Requests {
    class PatchInformationRequest: BaseRouterRequest {
        public PatchInformationRequest(byte[] packets) : base(packets) { }

        public override byte[] GetResponse() {
            WriteHeader();

            foreach (KeyValuePair<string, string> p in XMLManager.GetPatch()) {
                // Patch IP
                PacketWriter.WriteString(p.Key);
                // Patch port
                PacketWriter.WriteString(p.Value);
            }

            PacketWriter.WriteShort(0);
            PacketWriter.WriteByte(112);
            PacketWriter.WriteString("93");
            PacketWriter.WriteByte(12);

            return PacketWriter.ToArray();
        }
    }
}
