﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOfflineRouter.Requests {    
    class RequestRouter {
        private static readonly Dictionary<RequestTypes, Type> requestMap = new Dictionary<RequestTypes, Type>() {
            { RequestTypes.HelloWorld, typeof(HelloWorldRequest) },
            { RequestTypes.Update, typeof(UpdateRequest) },
        };

        public static void HandleRequest(Client cli, byte[] packets, int packetId) {
            try {
                Type handlerType = requestMap[(RequestTypes)packetId];
                BaseRouterRequest request = Activator.CreateInstance(handlerType, packets) as BaseRouterRequest;                
                byte[] response = request.GetResponse();

                cli.Send(ref response);
            }
            catch (Exception e) {
                Console.WriteLine("Unable to handle request with packet ID {0}, payload: {1}", packetId, packets);
            }
        }
    }
}
