﻿using RealmOffline.Accounts;
using RealmOffline.Base;
using RealmOffline.Core.Rooms;
using RealmOffline.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.TCP {
    static class RoomBroadcaster {
        public static void SendToRoomExcludingSelf(Room room, BaseMessage message, Account self) {
            //todo, when moving to more DB stuff get players from database state
            foreach (Character c in room.Players) {                
                Account a = ServerGlobals.GetAccountFromCharacter(c);

                if (c != null && !IsEqualAccount(a, self)) {
                    message.Account = a;
                    byte[] messageBytes = message.GetMessageBytes();

                    Console.WriteLine("Broadcasting message {0} to room {1} and user {2}", message.GetType(), room.RoomID, a.AccountId);
                    a.Socket.Send(ref messageBytes);
                }
            }
        }

        public static void SendToRoom(Room room, BaseMessage message) {
            SendToRoomExcludingSelf(room, message, null);
        }

        private static bool IsEqualAccount(Account a, Account b) {
            if (a == null || b == null)
                return false;

            return a.AccountId == b.AccountId;
        }
    }
}
