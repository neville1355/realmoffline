﻿using RealmOffline.Accounts;
using RealmOffline.Base;
using RealmOffline.Core;
using RealmOffline.Core.Rooms;
using RealmOffline.Messages;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using RealmOffline.TCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Commands {
    class TeleportToRoomCommand: BaseCommand {
        public Room DestinationRoom {
            get {
                uint roomNum = 0;
                uint.TryParse(this.CommandArgs[1], out roomNum);

                return ScriptResolver.GetRoom(roomNum);
            }
        }
        public TeleportToRoomCommand(WorldClient client, byte[] packets) : base(client, packets) {}

        public override void SendResponse() {
            if (!IsValidArgs())
                return;

            Account account = Client.GameAccount;
            Character character = account.CurrentCharacter;
            Room currentRoom = character.Location.CurrentRoom;

            RemoveFromRoomMessage removeMessage = new RemoveFromRoomMessage(account, currentRoom, character);
            RoomBroadcaster.SendToRoomExcludingSelf(currentRoom, removeMessage, account);
            currentRoom.Players.Remove(character);

            DestinationRoom.Players.Add(character);
            character.Location.CurrentRoom = DestinationRoom;
            AddToRoomMessage addMessage = new AddToRoomMessage(account, character);
            RoomBroadcaster.SendToRoomExcludingSelf(DestinationRoom, addMessage, account);

            TeleportMovieMessage teleportMovieMsg = new TeleportMovieMessage(account, character, currentRoom, DestinationRoom);
            byte[] teleportMsgBytes = teleportMovieMsg.GetMessageBytes();
            
            Client.Send(ref teleportMsgBytes);
        }

        private bool IsValidArgs() {
            bool valid = true;
            byte[] errorReply;

            if (this.CommandArgs[1].ToLower() == "list") {
                valid = false;
                // try to build this crazy packet
                StringBuilder l = new StringBuilder();
                l.AppendLine("Current Rooms. ");
                for (int i = 0; i < ScriptResolver.ImportedRooms.Count; i++) {
                    if (i == ScriptResolver.ImportedRooms.Count - 1)
                        l.Append(string.Format("{0}.", ScriptResolver.ImportedRooms[i].RoomID));
                    else
                        l.Append(string.Format("{0}-", ScriptResolver.ImportedRooms[i].RoomID));
                }
                byte[] roomList = Packet.ChatPacket(1, 1, l.ToString(), "RoomList");
                Client.Send(ref roomList);
            }
            else // if second arg is not list, then we only accept a room number
            {
                uint roomNum = 0;
                if (uint.TryParse(this.CommandArgs[1], out roomNum)) {
                    Room r = null;
                    // do we have the room ?
                    foreach (Room room in ScriptResolver.ImportedRooms) {
                        if (room.RoomID == roomNum)
                            r = room;
                    }
                    if (r == null) {
                        valid = false;
                        errorReply = Packet.ChatPacket(1, 1, "Invalid room number requested.", "Teleport");
                        Client.Send(ref errorReply);
                    }
                    else if (roomNum == Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID) {
                        valid = false;
                        errorReply = Packet.ChatPacket(1, 1, "You are already in that room..", "Teleport");
                        Client.Send(ref errorReply);
                    }
                }
                else {
                    valid = false;
                    errorReply = Packet.ChatPacket(1, 1, "Invalid room number requested.", "Teleport");
                    Client.Send(ref errorReply);
                }
            }

            return valid;
        }
    }
}
