﻿using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Commands {
    abstract class BaseCommand {
        protected WorldClient Client { get; }
        protected byte[] Packets { get; }
        protected string[] CommandArgs { get; }
        protected string CommandData { get; }

        public BaseCommand(WorldClient client, byte[] packets) {
            Client = client;
            Packets = packets;

            CommandArgs = GetCommandArgs();
            CommandData = GetCommandData();
        }

        public abstract void SendResponse();

        private string[] GetCommandArgs() {
            byte[] trimmedMessage = new byte[Packets.Length - 8];
            Array.Copy(Packets, 8, trimmedMessage, 0, trimmedMessage.Length);
            string command = Encoding.ASCII.GetString(trimmedMessage);
                        
            return CleanCommandText(command).Split(' ');
        }

        private string GetCommandData() {
            StringBuilder commandDataBuilder = new StringBuilder();
            for (int i = 1; i < this.CommandArgs.Length; i++) {
                commandDataBuilder.Append(this.CommandArgs[i] + " ");
            }

            return commandDataBuilder.ToString();
        }

        private string CleanCommandText(string text) {
            byte[] cmdText = Encoding.ASCII.GetBytes(text);

            while (cmdText[cmdText.Length - 1] == 0) {
                byte[] newArray = new byte[cmdText.Length - 1];
                Array.Copy(cmdText, 0, newArray, 0, newArray.Length);
                cmdText = newArray;
            }
            return Encoding.ASCII.GetString(cmdText);
        }
    }
}
