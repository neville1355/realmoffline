﻿using MySql.Data.MySqlClient;
using RealmOffline.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Data {
    class CharacterDeleter: MySqlTransactionBase {
        private Character Character { get; set; }
        public CharacterDeleter(Character character) {
            Character = character;
        }

        public static void Delete(Character character) {
            CharacterDeleter deleter = new CharacterDeleter(character);
            deleter.Delete();
        }

        public void Delete() {
            try {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = @"
                            DELETE FROM BCharacter WHERE WorldObjectId=@Id;
                            DELETE FROM BHead WHERE WorldObjectId=@Id;
                            DELETE FROM BWeapon WHERE OwnerId=@Id;
                            DELETE FROM skills WHERE CharId=@Id;
                            DELETE FROM spells WHERE CharId=@Id;
                            DELETE FROM states WHERE CharId=@Id;
                            DELETE FROM Inventory WHERE SqlOwnerId=@Id;
                            DELETE FROM affects WHERE CharId=@Id;
                            DELETE FROM BContain WHERE OwnerId=@Id;
                            DELETE FROM mail WHERE CharId=@Id;
                            DELETE FROM WorldObject WHERE Id=@Id;
                            ";

                cmd.Parameters.AddWithValue("@Id", Character.SqlCharId);

                ExecuteInTransaction(cmd);
                CommitTransaction();
            }
            catch (Exception e) {
                RollbackTransaction();
                throw e;
            }

        }
    }
}
