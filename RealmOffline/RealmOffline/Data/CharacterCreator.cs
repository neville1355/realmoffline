﻿using MySql.Data.MySqlClient;
using RealmOffline.Accounts;
using RealmOffline.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace RealmOffline.Data {
    public class CharacterCreator: MySqlTransactionBase {
        private Character Character { get; set; }
        
        public CharacterCreator(Character character) {
            Character = character;
        }

        public static void Create(Character character) {
            CharacterCreator creator = new CharacterCreator(character);
            creator.Create();
        }

        public void Create() {
            try {
                Character.SqlCharId = CreateWorldObject();
                CreateBCharacter();
                CreateBHead();
                CreateSkills();
                CreateSpells();
                CreateStates();
                CreateAffects();
                CommitTransaction();
            }
            catch (Exception e) {
                RollbackTransaction();
                throw e;
            }
        }

        private void CreateAffects() {
            string commandText = @"
                INSERT INTO Affects(CharId) VALUES(@CharId)
            ";

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.AddWithValue("@CharId", Character.SqlCharId);

            ExecuteInTransaction(cmd);
        }

        private void CreateSkills() {
            string commandText = @"
                INSERT INTO Skills(CharId) VALUES(@CharId)
            ";

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.AddWithValue("@CharId", Character.SqlCharId);

            ExecuteInTransaction(cmd);
        }

        private void CreateSpells() {
            string commandText = @"
                INSERT INTO Spells(CharId) VALUES(@CharId)
            ";

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.AddWithValue("@CharId", Character.SqlCharId);
            ExecuteInTransaction(cmd);
        }

        private void CreateStates() {
            string commandText = @"
                INSERT INTO States(CharId) VALUES(@CharId)
            ";

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.AddWithValue("@CharId", Character.SqlCharId);
            ExecuteInTransaction(cmd);
        }

        private void CreateBHead() {
            string commandText = @"
                INSERT INTO BHead(Race, Sex, HeadNumber, EyeNumber, HairNumber, BrowNumber, NoseNumber, EarNumber, MouthNumber, FaceHairNumber,
                                  SkinColor, EyeColor, HairColor, WorldObjectId)
                           VALUES(@Race, @Sex, @HeadNumber, @EyeNumber, @HairNumber, @BrowNumber, @NoseNumber, @EarNumber, @MouthNumber, @FaceHairNumber,
                                  @SkinColor, @EyeColor, @HairColor, @WorldObjectId)
            ";

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.AddWithValue("@Race", Character.RaceHead);
            cmd.Parameters.AddWithValue("@Sex", Character.Gender);
            cmd.Parameters.AddWithValue("@HeadNumber", Character.HeadPart);
            cmd.Parameters.AddWithValue("@EyeNumber", Character.EyePart);
            cmd.Parameters.AddWithValue("@HairNumber", Character.HairPart);
            cmd.Parameters.AddWithValue("@BrowNumber", Character.EyeBrowPart);
            cmd.Parameters.AddWithValue("@NoseNumber", Character.NosePart);
            cmd.Parameters.AddWithValue("@EarNumber", Character.EarPart);
            cmd.Parameters.AddWithValue("@MouthNumber", Character.MouthPart);
            cmd.Parameters.AddWithValue("@FaceHairNumber", Character.FacialHairPart);
            cmd.Parameters.AddWithValue("@SkinColor", Character.SkinColor);
            cmd.Parameters.AddWithValue("@EyeColor", Character.EyeColor);
            cmd.Parameters.AddWithValue("@HairColor", Character.HairColor);
            cmd.Parameters.AddWithValue("@WorldObjectId", Character.SqlCharId);
            ExecuteInTransaction(cmd);
        }

        private void CreateBCharacter() {
            string commandText = @"
                INSERT INTO BCharacter(Name, Title, Profession, Experience, Sex, BuildPoints, Peaceful, WorldObjectId, AccountId, Race)
                                    VALUES(@Name, @Title, @Profession, @Experience, @Sex, @BuildPoints, @Peaceful, @WorldObjectId, @AccountId, @Race)
            ";

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.AddWithValue("@Name", Character.Name);
            cmd.Parameters.AddWithValue("@Title", Character.Title);
            cmd.Parameters.AddWithValue("@Profession", Character.Class);
            cmd.Parameters.AddWithValue("@Experience", Character.Experience);
            cmd.Parameters.AddWithValue("@Sex", Character.Gender);
            cmd.Parameters.AddWithValue("@BuildPoints", Character.BuildPoints);
            cmd.Parameters.AddWithValue("@Peaceful", Character.PvpSwitch);
            cmd.Parameters.AddWithValue("@WorldObjectId", Character.SqlCharId);
            cmd.Parameters.AddWithValue("@AccountId", Character.AccountOwnerId);
            cmd.Parameters.AddWithValue("@Race", Character.Race);
            ExecuteInTransaction(cmd);
        }

        private uint CreateWorldObject() {
            uint worldObjectId = 0;
            string commandText = @"
                INSERT INTO WorldObject(XSpeed, YSpeed, Strength, Dexterity, Intelligence, Endurance, Level, Health, MaxHealth, Alignment,
                                        Armor, BaseArmor, Hunger, Thirst, CreationTime, ManaValue, RoomNumber, ArmorType, Stamina, 
                                        Girth, Height, IsContainer, IsCharacter, IsHead)
                                    VALUES(@XSpeed, @YSpeed, @Strength, @Dexterity, @Intelligence, @Endurance, @Level, @Health, @MaxHealth, @Alignment,
                                        @Armor, @BaseArmor, @Hunger, @Thirst, @CreationTime, @ManaValue, @RoomNumber, @ArmorType, @Stamina, 
                                        @Girth, @Height, @IsContainer, @IsCharacter, @IsHead)
            ";

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.AddWithValue("@Id", 0);
            cmd.Parameters.AddWithValue("@XSpeed", Character.DEFAULT_X_SPEED);
            cmd.Parameters.AddWithValue("@YSpeed", Character.DEFAULT_Y_SPEED);
            cmd.Parameters.AddWithValue("@Strength", Character.Strength);
            cmd.Parameters.AddWithValue("@Dexterity", Character.Dexterity);
            cmd.Parameters.AddWithValue("@Intelligence", Character.Intelligence);
            cmd.Parameters.AddWithValue("@Endurance", Character.Constitution);
            cmd.Parameters.AddWithValue("@Level", 1);
            cmd.Parameters.AddWithValue("@Health", Character.TotalHP);
            cmd.Parameters.AddWithValue("@MaxHealth", Character.TotalHP);
            cmd.Parameters.AddWithValue("@Alignment", Character.Alignment);
            cmd.Parameters.AddWithValue("@Armor", 0);//not sure where to derrive this from yet
            cmd.Parameters.AddWithValue("@BaseArmor", 0);
            cmd.Parameters.AddWithValue("@Hunger", 0);
            cmd.Parameters.AddWithValue("@Thirst", 0);
            cmd.Parameters.AddWithValue("@CreationTime", 0); //todo fix this
            cmd.Parameters.AddWithValue("@ManaValue", Character.Mana);
            cmd.Parameters.AddWithValue("@RoomNumber", 5043); //hack for now
            cmd.Parameters.AddWithValue("@ArmorType", 0);//not sure what this is supposed to be really
            cmd.Parameters.AddWithValue("@Stamina", 100); //not impl yet
            cmd.Parameters.AddWithValue("@IsContainer", 1);
            cmd.Parameters.AddWithValue("@IsCharacter", 1);
            cmd.Parameters.AddWithValue("@IsHead", 1);
            cmd.Parameters.AddWithValue("@Girth", Character.Girth);
            cmd.Parameters.AddWithValue("@Height", Character.Height);
            ExecuteInTransaction(cmd);
            worldObjectId = (uint)cmd.LastInsertedId;

            return worldObjectId;
        }
    }
}
 