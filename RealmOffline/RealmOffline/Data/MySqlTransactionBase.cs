﻿using MySql.Data.MySqlClient;
using RealmOffline.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Data {
    public abstract class MySqlTransactionBase: IDisposable {
        private readonly static XMLManager xml = new XMLManager();

        private MySqlConnection connection;
        private MySqlConnection Connection {
            get {
                if (connection == null || connection.State == System.Data.ConnectionState.Closed) {
                    connection = new MySqlConnection(xml.GetMySqlConnection);
                    connection.Open();
                }

                return connection;
            }
        }

        private MySqlTransaction transaction;
        private MySqlTransaction Transaction {
            get {
                if (transaction == null)
                    transaction = Connection.BeginTransaction();

                return transaction;
            }
        }

        public void Dispose() {
            if (Connection.State != System.Data.ConnectionState.Closed)
                CloseConnection();
        }

        protected int ExecuteInTransaction(MySqlCommand command) {
            command.Connection = Connection;
            command.Transaction = Transaction;

            return command.ExecuteNonQuery();
        }

        protected MySqlDataReader GetReader(MySqlCommand cmd) {
            cmd.Connection = Connection;
            return cmd.ExecuteReader();
        }

        protected void CommitTransaction() {
            Transaction.Commit();
            CloseConnection();
        }

        protected void RollbackTransaction() {
            Transaction.Rollback();
            CloseConnection();
        }

        protected void CloseConnection() {
            Connection.Close();
        }

        protected T GetColumnValue<T>(MySqlDataReader reader, string columnName) {
            object value = reader[columnName];
            if (value == DBNull.Value)
                return default(T);
            else
                return (T)Convert.ChangeType(value, typeof(T));
        }
    }
}
