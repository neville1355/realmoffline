﻿using RealmOffline.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class BaseMessagePart : IMessagePart {
        private PacketWriter packets;
        protected PacketWriter Packets {
            get {
                //packets gets closed after ToArray() is called because its internal stream is closed
                //we want to ability to re-open to chain many messages to broadcast to many accounts
                if (packets == null || packets.IsClosed)
                    packets = new PacketWriter();

                return packets;
            }
        }

        /// <summary>
        /// Override this in derrived classes to commit writing to the packet writer
        /// </summary>
        protected virtual void WritePackets() { }

        public byte[] GetMessageBytes() {
            WritePackets();
            return Packets.GetRawPacket();
        }
    }
}
