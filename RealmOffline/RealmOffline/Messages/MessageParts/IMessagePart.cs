﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    interface IMessagePart {
        byte[] GetMessageBytes();
    }
}
