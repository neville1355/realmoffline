﻿using RealmOffline.Core;
using RealmOffline.Core.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class EntityLocationMessagePart: BaseMessagePart {
        public IEntity Entity { get; }
        public EntityLocationMessagePart(IEntity entity) : base() {
            Entity = entity;            
        }

        protected override void WritePackets() {
            IRoom currentRoom = Entity.Location.CurrentRoom;

            if (currentRoom.RoomID < ushort.MaxValue) {
                Packets.WriteByte(0x01);
                Packets.WriteUShort((ushort)currentRoom.RoomID);
            }
            else {
                Packets.WriteByte(0x02);
                Packets.WriteUInt32(currentRoom.RoomID);
            }

            Packets.WriteUShort(Entity.Location.X);
            Packets.WriteUShort(Entity.Location.Y);
            Packets.WriteUShort(Entity.Location.Facing);
        }
    }
}
