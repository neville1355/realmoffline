﻿using RealmOffline.Accounts;
using RealmOffline.Core.Entities;
using RealmOffline.Core.Items.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class InventoryInfoMessagePart: BaseMessagePart {
        public IInventoryEntity Entity { get; }
        public InventoryInfoMessagePart(IInventoryEntity entity) : base() {
            Entity = entity;
        }

        protected override void WritePackets() {
            List<BaseGameItem> equipped = Entity.Inventory.Where(i => i.Equipped).ToList();
            Packets.WriteUShort((ushort)(equipped.Count + 1)); // +1 for head information i think?
            
            //todo find out what all these bytes map to?!?!
            Packets.WriteByte(0x01);
            Packets.WriteUShort(0x04);
            Packets.WriteUInt32(Entity.InventoryId);
            /* This is not fully understood
             * Your inventory is alot like a char and or a item
             * it may be able to hold buffs.
             */
            Packets.WriteBytes(new byte[] { 0x00, 0x00, 0x00, 0xFF });
            Packets.WriteBytes(new byte[3]);

            //this is a hack because we treat game items as something other than basic entities right now
            //but I believe a character's head is actually considered part of the inventory?? (shrug)
            if (Entity is ICharacterEntity) {
                ICharacterEntity character = Entity as ICharacterEntity;
                Packets.WriteBytes(new CharacterHeadInfoMessagePart(character).GetMessageBytes());
            }
            else {
                Packets.WriteBytes(new byte[13]);
            }

            Packets.WriteBytes(new byte[6]);

            foreach (BaseGameItem item in equipped) {
                Packets.WriteBytes(item.Serialize());
            }
        }
    }
}
