﻿using RealmOffline.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class CharacterHeadInfoMessagePart : BaseMessagePart {
        public ICharacterEntity Character { get; }
        public CharacterHeadInfoMessagePart(ICharacterEntity character) : base() {
            Character = character;
        }

        protected override void WritePackets() {
            Packets.WriteByte(Character.Race);
            Packets.WriteByte(Character.Gender);
            Packets.WriteByte(Character.ChinPart);
            Packets.WriteByte(Character.HairPart);
            Packets.WriteByte(Character.EyeBrowPart);
            Packets.WriteByte(Character.EyePart);
            Packets.WriteByte(Character.NosePart);
            Packets.WriteByte(Character.EarPart);
            Packets.WriteByte(Character.MouthPart);
            Packets.WriteByte(Character.FacialHairPart);
            Packets.WriteByte(Character.SkinColor);
            Packets.WriteByte(Character.HairColor);
            Packets.WriteByte(Character.EyeColor);
        }
    }
}
