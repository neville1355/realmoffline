﻿using RealmOffline.Accounts;
using RealmOffline.Core.Entities;
using RealmOffline.Core.Rooms;
using RealmOffline.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    /// <summary>
    /// this is a fragment of a message, not a full packet
    /// 
    /// It's separated out for composability for other messages
    /// </summary>
    class CharacterInfoMessagePart : BaseMessagePart {
        public Character Character { get; }
        public IRoom Room { get; }
        public CharacterInfoMessagePart(Character character) : base() {
            Character = character;
        }

        protected override void WritePackets() {
            Packets.WriteByte(Character.XSpeed);
            Packets.WriteByte(Character.YSpeed);
            Packets.WriteByte(Character.Girth);
            Packets.WriteByte(Character.Height);
            Packets.WriteByte(Character.Class);
            Packets.WriteByte(Character.Race);
            Packets.WriteByte(Character.Gender);
            Packets.WriteString(Character.Name);
            Packets.WriteByte(Character.PvpSwitch);
            Packets.WriteInt32(Character.CurrentHP);
            Packets.WriteInt32(Character.TotalHP);
        }
    }
}
