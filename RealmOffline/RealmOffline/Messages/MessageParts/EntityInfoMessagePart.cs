﻿using RealmOffline.Accounts;
using RealmOffline.Core;
using RealmOffline.Core.Entities;
using RealmOffline.Core.Rooms;
using RealmOffline.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class EntityInfoMessagePart: BaseMessagePart {
        public Character Entity { get; }
        public IRoom Room { get; }
        public EntityInfoMessagePart(Character worldObject, IRoom room) : base() {
            Entity = worldObject;
            Room = room;
        }

        protected override void WritePackets() {
            PacketWriter wrapper = new PacketWriter();

            WriteMessageHeader(wrapper);
            WriteActiveEffects(wrapper);
            wrapper.WriteByte(0); //always 0, it is 1 if money ( i think )

            if (Entity is ICharacterEntity)
                wrapper.WriteBytes(new CharacterInfoMessagePart(Entity as Character).GetMessageBytes());

            if(Room != null)
                wrapper.WriteBytes(new EntityLocationMessagePart(Entity).GetMessageBytes());

            if (Entity is IInventoryEntity)
                wrapper.WriteBytes(new InventoryInfoMessagePart(Entity as IInventoryEntity).GetMessageBytes());
            WriteMessageFooter(wrapper);
        }

        private void WriteMessageHeader(PacketWriter wrapper) {
            wrapper.WriteUInt32(Entity.GameID); //who is being added to the room.. redundant??

            //world object packets start            
            wrapper.WriteUShort(Entity.ClassType);
            wrapper.WriteByte(0); //crash without this
            wrapper.WriteUInt32(Entity.GameID); //subject of the movie
            wrapper.WriteByte(0); //spacer, always zero for characters (different for npc)
        }

        private void WriteMessageFooter(PacketWriter wrapper) {
            byte[] wrappedMessage = wrapper.ToArray(false, true);
            Packets.WriteBytes(wrappedMessage);
            Packets.WriteBytes(new byte[] { 0x00, 0x00, 0x00, 0xFF });
        }

        private void WriteActiveEffects(PacketWriter wrapper) {
            //not sure what an "active effect" is, but 255 is the terminator for it
            wrapper.WriteByte(255); //255 signals end of head packets
        }
    }
}
