﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    interface IMessage : IMessagePart {
        Requests.RequestTypes Type { get; }
    }
}
