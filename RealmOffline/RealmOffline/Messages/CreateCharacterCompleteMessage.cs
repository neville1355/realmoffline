﻿using RealmOffline.Accounts;
using RealmOffline.Base;
using RealmOffline.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class CreateCharacterCompleteMessage : AckMessage {
        public Character Character { get; }
        public CreateCharacterCompleteMessage(Account account, Character character) : base(account, Requests.RequestTypes.BeginCharacterCreation) {
            Character = character;
        }
        protected override void WritePackets() {
            base.WritePackets();

            PacketWriter wrapper = new PacketWriter();

            WriteMessageHeader(wrapper);
            WriteActiveEffects(wrapper);
            wrapper.WriteByte(0); //always 0, it is 1 if money ( i think )

            wrapper.WriteBytes(new CharacterInfoMessagePart(Character).GetMessageBytes());            
            wrapper.WriteBytes(new InventoryInfoMessagePart(Character).GetMessageBytes());//WriteEquipmentInformation(wrapper);

            WriteMessageFooter(wrapper);
        }

        private void WriteMessageHeader(PacketWriter wrapper) {
            wrapper.WriteUInt32(Character.GameID); //who is being added to the room.. redundant??

            //world object packets start            
            wrapper.WriteUShort(Character.ClassType);
            wrapper.WriteByte(0); //crash without this
            wrapper.WriteUInt32(Character.GameID); //subject of the movie
            wrapper.WriteByte(0); //spacer, always zero for characters (different for npc)
        }

        private void WriteMessageFooter(PacketWriter wrapper) {
            byte[] wrappedMessage = wrapper.ToArray(false, true);
            Packets.WriteBytes(wrappedMessage);
            Packets.WriteBytes(new byte[] { 0x00, 0x00, 0x00, 0x2E }); //why this is 2E instead of FF is not clear
        }

        private void WriteActiveEffects(PacketWriter wrapper) {
            //not sure what an "active effect" is, but 255 is the terminator for it
            wrapper.WriteByte(255); //255 signals end of head packets
        }
    }
}
