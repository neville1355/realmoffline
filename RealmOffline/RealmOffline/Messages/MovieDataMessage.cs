﻿using RealmOffline.Base;
using RealmOffline.Core.Rooms;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class MovieDataMessage : BaseMessage, IRoomBasedMessage {
        public const int OWNER_SELF = -1;
        public MovieTypes MovieType { get; }
        public int OwnerId { get; set; }
        public int ServId { get; set; } //da fuq is this        
        public IRoom Room { get; set; }

        public MovieDataMessage(Account account, MovieTypes movieType) : base(account, Requests.RequestTypes.MovieData) {
            MovieType = movieType;
            OwnerId = OWNER_SELF;
            Room = Account.CurrentCharacter.Location.CurrentRoom;
        }

        protected override void WritePackets() {
            base.WritePackets();

            Packets.WriteInt32(-1); //this packet might need to be the character id on the account as well
            Packets.WriteUInt32(Room.RoomID);
            Packets.WriteByte((byte)MovieType);
            //now allow the derrived class to write the remaining bytes about its movie data
        }
    }
}
