﻿using RealmOffline.Core.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    interface IRoomBasedMessage: IMessage {
        IRoom Room { get; }
    }
}
