﻿using RealmOffline.Base;
using RealmOffline.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    public abstract class BaseMessage : IMessage {
        public Requests.RequestTypes Type { get; }
        public Account Account { get; set; }

        private PacketWriter packets;
        protected PacketWriter Packets {
            get {
                //packets gets closed after ToArray() is called because its internal stream is closed
                //we want to ability to re-open to chain many messages to broadcast to many accounts
                if (packets == null || packets.IsClosed)
                    packets = new PacketWriter((int)Type);

                return packets;
            }
        }
        public BaseMessage(Account account, Requests.RequestTypes type) {
            Account = account;
            Type = type;
        }

        /// <summary>
        /// Override this in derrived classes to commit writing to the packet writer
        /// </summary>
        protected virtual void WritePackets() {
            Packets.WriteUInt32(Account.AccountId);
            Packets.WriteUInt32(0);
        }

        public byte[] GetMessageBytes() {
            WritePackets();
            return Packets.ToArray();
        }
    }
}
