﻿using RealmOffline.Core;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class ShowMagicMailRequest: BaseWorldRequest {
        public ShowMagicMailRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            //  Console.WriteLine("Start 0x87");
            PacketWriter w = new PacketWriter(25);
            w.WriteUInt32(Client.GameAccount.AccountId);
            w.WriteInt32(0);
            w.WriteInt32(0x87);
            w.WriteInt32(0);
            w.WriteShort(0);
            // get all mail from sql

            if (Client.GameAccount.CurrentCharacter.GameMail == null)
                Client.GameAccount.CurrentCharacter.GameMail = new MagicMail(Client.GameAccount);
            // Refresh mail
            Client.GameAccount.CurrentCharacter.GameMail.RefreshMail();

            if (Client.GameAccount.CurrentCharacter.GameMail.AllMail == null)
                Client.GameAccount.CurrentCharacter.GameMail.AllMail = new List<Mail>();
            // Console.WriteLine("{0} mails for char {1}", Client.GameAccount.CurrentCharacter.GameMail.AllMail.Count, Client.GameAccount.CurrentCharacter.CharId);
            w.WriteShort((short)Client.GameAccount.CurrentCharacter.GameMail.AllMail.Count);

            w.WriteByte(0x01); // start mail data

            if (Client.GameAccount == null) Console.WriteLine("Game acct null.");
            if (Client.GameAccount.CurrentCharacter == null) Console.WriteLine("Char is null");
            if (Client.GameAccount.CurrentCharacter.GameMail == null) Console.WriteLine("Mail Container null");
            if (Client.GameAccount.CurrentCharacter.GameMail.AllMail == null) Console.WriteLine("All mail is null");
            foreach (Mail m in Client.GameAccount.CurrentCharacter.GameMail.AllMail) {
                w.WriteInt32(m.MailID);
                w.WriteByte(0x00);
                w.WriteInt32(m.TimeStamp);
                byte[] subject = Encoding.ASCII.GetBytes(m.Subject);
                w.WriteShort((short)subject.Length);
                w.WriteBytes(subject);
                byte[] from = Encoding.ASCII.GetBytes(m.From);
                w.WriteShort((short)from.Length);
                w.WriteBytes(from);
            }

            w.WriteBytes(new byte[] { 0x00, 0x00, 0xFF });
            byte[] reply = w.ToArray();
            Client.Send(ref reply);
        }
    }
}
