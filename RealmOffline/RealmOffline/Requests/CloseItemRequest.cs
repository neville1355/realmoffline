﻿using RealmOffline.Accounts;
using RealmOffline.Core.Items.Base;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class CloseItemRequest: BaseWorldRequest {
        public CloseItemRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Console.WriteLine("Close Item packet 156 (Close Items): {0}", BitConverter.ToString(Packets));
            Character c = Client.GameAccount.CurrentCharacter;
            Reader.ReadBytes(8); // pck length and id
                                 // the door id ?
            uint itemid = BitConverter.ToUInt16(Reader.ReadBytes(4), 0);

            Reader.Close();
            byte[] reply25 = Packet.ReplyWith25(Client.GameAccount.AccountId, 156,
                new byte[] { 0x02, 0x00, 0x21, 0xFF, 0x00 });
            Client.Send(ref reply25);
            PacketWriter w = new PacketWriter(42);
            w.WriteUInt32(Client.GameAccount.AccountId);
            w.WriteInt32(0);
            w.WriteUInt32(c.GameID);
            w.WriteUInt32(c.Location.CurrentRoom.RoomID);
            w.WriteByte(0x0B);
            w.WriteUInt32(c.GameID);
            w.WriteUInt32(itemid);
            w.WriteBytes(new byte[] { 0x21, 0xFF, 0x00, });
            byte[] reply42 = w.ToRealmPacket();
            foreach (BaseGameItem i in Client.GameAccount.CurrentCharacter.Location.CurrentRoom.Items) {
                if (i is Door && i.GameID == itemid) {
                    Door d = i as Door;
                    if (d.IsOpen == false)
                        d.IsOpen = true;
                    else d.IsOpen = false;
                }
            }
            Client.Send(ref reply42);
        }
    }
}
