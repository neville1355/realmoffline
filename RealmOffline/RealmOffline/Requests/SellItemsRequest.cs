﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class SellItemsRequest: BaseWorldRequest {
        public SellItemsRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Reader.ReadBytes(8);
            uint charid = Reader.ReadUInt32();
            uint itemid = Reader.ReadUInt32();
            Reader.Close();
            //    Console.WriteLine("Char {0} wants to sell item {1}", charid, itemid);
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0x00);
            w25.WriteUInt32(0x4A);
            w25.WriteInt32(750000); // how much gold to give
            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);
        }
    }
}
