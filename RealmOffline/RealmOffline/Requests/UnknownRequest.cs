﻿using RealmOffline.Accounts;
using RealmOffline.Core;
using RealmOffline.Core.Rooms;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    /// <summary>
    /// This class is a dump of packets that used to be in the giant switch statement in RealmPacketIO
    /// 
    /// It represents packets that do not have a known mapping yet
    /// 
    /// In a perfect world, this class will eventually be deleted.
    /// </summary>
    class UnknownRequest {
        public static void HandleRequest(WorldClient cli, byte[] pak, int pid) {
            MemoryStream stream = new MemoryStream(pak);
            BinaryReader reader = new BinaryReader(stream);

            switch (pid) {
                #region 0x0F
                case 0x0F: // no clue why the client sends this packet, we do not need to reply to it
                    //Console.WriteLine("Sent 0x0F");
                    break;
                #endregion
                #region  0x10
                case 0x10: {
                        //  Console.WriteLine("Start 0x10");
                        PacketWriter w = new PacketWriter(25);
                        w.WriteUInt32(cli.GameAccount.AccountId);
                        w.WriteInt32(0);
                        w.WriteInt32(0x15);
                        w.WriteBytes(new byte[] { 0x21, 0xE5, 0x30, 0x40 });
                        byte[] reply = w.ToArray();
                        cli.Send(ref reply);
                        //  Console.WriteLine("Sent 0x10");
                    }
                    break;
                #endregion
                #region 0x15 I do not know but works
                case 0x15: {
                        //     Console.WriteLine("Start 0x15");
                        PacketWriter w = new PacketWriter(0x15);
                        w.WriteUInt32(cli.GameAccount.AccountId);
                        w.WriteUInt32(0);
                        w.WriteUInt32(0x15);
                        w.WriteBytes(new byte[] { 0x21, 0xE5, 0x30, 0x40 });
                        byte[] r = w.ToArray();
                        cli.Send(ref r);
                        //  Console.WriteLine("Sent 0x15");
                    }
                    break;
                #endregion
                #region 0x16 I dunno but works
                case 0x16: {
                        // When client clicks quit the correct way, this packet is sent.
                        // which client cant recieve, because it has quit.
                        //Console.WriteLine("Packet 22 (0x16) from client not answered {0}", BitConverter.ToString(pak)); 
                        //  Console.WriteLine("Sent 0x16");
                    }
                    break;
                #endregion
                #region 0x17 I dunno but works.
                case 0x17: {
                        //    Console.WriteLine("Start 0x17");
                        PacketWriter w = new PacketWriter(0x17);
                        w.WriteUInt32(cli.GameAccount.AccountId);
                        w.WriteUInt32(0);
                        w.WriteUInt32(0x17);
                        w.WriteUInt32(0);
                        byte[] r = w.ToArray();
                        cli.Send(ref r);
                        //  Console.WriteLine("Sent 0x17");
                    }
                    break;
                #endregion
                #region 0x37 Look At 55
                case 0x37: {
                        //  Console.WriteLine("Start 55");

                        reader.ReadBytes(4);// len
                        reader.ReadBytes(4);// id
                        uint charId = BitConverter.ToUInt32(reader.ReadBytes(4), 0);
                        reader.Close();
                        // Console.WriteLine("Look at {0}", charId);
                        Entity e = null;
                        if (cli.GameAccount.CurrentCharacter != null) {
                            if (cli.GameAccount.CurrentCharacter.Location != null) {
                                if (cli.GameAccount.CurrentCharacter.Location.CurrentRoom != null) {
                                    e = cli.GameAccount.CurrentCharacter.Location.CurrentRoom.GetEntityInRoom(charId);
                                }
                            }
                        }
                        if (e == null) {
                            // Must be looking at on screen
                            e = cli.GameAccount.GetCharFromGameID(charId);
                        }
                        if (e is Character) {
                            Character c = e as Character;
                            byte[] reply = c.CreateLookAt55(c.GameID);

                            cli.Send(ref reply);
                        }
                        //   Console.WriteLine("Sent 55");
                    }
                    break;
                #endregion
                #region 0x3A Look At 58
                case 0x3A: {
                        //         Console.WriteLine("Sending 58");
                        // Send packet 25 its big
                        // when we click play, this is the first packet sent. 
                        //Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Look At 58: {0}", BitConverter.ToString(pak));
                        Console.WriteLine("Total chars {0}.", cli.GameAccount.Characters.Count);
                        //Console.ResetColor();
                        reader.ReadBytes(4); // len
                        reader.ReadBytes(4); // id
                        uint charID = BitConverter.ToUInt32(reader.ReadBytes(4), 0);
                        Character c = null;
                        if (cli.GameAccount.CurrentCharacter == null)
                            cli.GameAccount.CurrentCharacter = cli.GameAccount.GetCharFromGameID(charID);

                        if (cli.GameAccount.CurrentCharacter.Location != null) {
                            if (cli.GameAccount.CurrentCharacter.Location.CurrentRoom != null) {
                                Entity e = cli.GameAccount.CurrentCharacter.Location.CurrentRoom.GetEntityInRoom(charID);
                                if (e is Character) {
                                    c = e as Character;
                                    byte[] look = c.CreateLookAt58(cli.GameAccount.AccountId);
                                    cli.Send(ref look);
                                }
                            }
                            else c = cli.GameAccount.CurrentCharacter;
                            // send yours
                        }
                        else {
                            if (c == null) c = cli.GameAccount.CurrentCharacter;
                            byte[] look = c.CreateLookAt58(cli.GameAccount.AccountId);
                            cli.Send(ref look);
                        }
                        /*
                        Character c = cli.GameAccount.GetCharFromGameID(charID);
                        if (c == null) c = cli.GameAccount.CurrentCharacter;
                        Console.WriteLine("We got char id {0} {1}", charID, c!= null ? "True":"False");
                        if (ServerGlobals.LoadFromSql)
                        {
                            byte[] charLookAt = c.CreateLookAt58(cli.GameAccount.AccountId);
                            // Ok we have the char, lets build a packet around his info
                            cli.Send(ref charLookAt);
                        }
                        else
                        {
                            Console.WriteLine("Sending fake look at");
                            byte[] fakeLook = GetByteArrayFromFileInBaseDir("lookat.txt");
                            cli.Send(ref fakeLook);
                        }
                        //done
                         */
                        //     Console.WriteLine("Sent 58");
                    }
                    break;
                #endregion
                #region 0x74 Look At 116
                case 0x74: {
                        //  Console.WriteLine("Start 116");
                        reader.ReadBytes(4); // len
                        reader.ReadBytes(4); // id
                        uint charID = BitConverter.ToUInt32(reader.ReadBytes(4), 0);
                        //   Console.WriteLine("Want to look at char id {0}: {1}", charID, BitConverter.ToString(pak));

                        Entity e = null;
                        if (cli.GameAccount.CurrentCharacter != null) {
                            if (cli.GameAccount.CurrentCharacter.Location != null) {
                                if (cli.GameAccount.CurrentCharacter.Location.CurrentRoom != null) {
                                    e = cli.GameAccount.CurrentCharacter.Location.CurrentRoom.GetEntityInRoom(charID);
                                }
                            }
                        }
                        if (e == null) {
                            // Must be looking at on screen 
                            e = cli.GameAccount.GetCharFromGameID(charID);
                        }
                        if (e is Character) {
                            Character c = e as Character;
                            byte[] reply = c.CreateLookAt116(c.GameID);

                            cli.Send(ref reply);
                        }

                        //done
                        // Console.WriteLine("Sent 116");
                    }
                    break;
                #endregion
                case 0x91: // 145 (Spell effect ?)
                    {
                        //Console.WriteLine("0x91 {0}", BitConverter.ToString(pak));
                        PacketWriter w = new PacketWriter(0x2A);
                        w.WriteUInt32(cli.GameAccount.AccountId);
                        w.WriteUInt32(0x00);
                        w.WriteUInt32(cli.GameAccount.CurrentCharacter.GameID);
                        w.WriteUInt32(cli.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
                        w.WriteBytes(new byte[] { 0x21, 0xFF, 0x00, 0x00 });
                        byte[] pw = w.ToArray();
                        cli.Send(ref pw);
                    }
                    break;
                case 0xA7: //??
                    {
                        // we dont know but the packet 25 is
                        PacketWriter w25 = new PacketWriter(0x19);
                        w25.WriteUInt32(cli.GameAccount.AccountId);
                        w25.WriteUInt32(0x00);
                        w25.WriteUInt32(0xA7);
                        w25.WriteBytes(new byte[] { 0x58, 0x23, 0x05, 0x08 });
                        byte[] p25 = w25.ToArray();
                        cli.Send(ref p25);
                    }
                    break;
                default: {
                        Console.WriteLine("WorldServer::Unknown Packet ID ({0})", pid);
                        Console.WriteLine(BitConverter.ToString(pak));
                    }
                    break;
            }
        }
    }
}
