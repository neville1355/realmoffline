﻿using RealmOffline.Accounts;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RealmOffline.Requests {
    class CreateCharacterDataRequest: BaseWorldRequest {
        public CreateCharacterDataRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            //Console.WriteLine("Start 59");
            // Console.WriteLine("Packet 59 {0}", BitConverter.ToString(pak));
            // 0x54, 0x65, 0x73, 0x74, 0x6f, 0x72, // Testor
            // Here during a new char creation we get our part data
            Character c = Client.GameAccount.CurrentCharacter;
            if (c == null) { Console.WriteLine("We have a null char, and we shouldnt"); }
            Reader.ReadBytes(4); // pck len
            Reader.ReadBytes(4); // pack id
            Reader.ReadBytes(4); // charid
                                 //Reader.ReadByte();
                                 //string fileLocation1 = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CharCreate0x3B20f3Client.txt");
                                 // File.WriteAllText(fileLocation1, BitConverter.ToString(pak));
                                 // Data starts
            c.AddPartData(Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte(),
                Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte(),
                Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte());
            Reader.Close();
            // Update the char mysql info

            PacketWriter writer = new PacketWriter(0x19);
            writer.WriteUInt32(Client.GameAccount.AccountId);
            writer.WriteUInt32(0x00);
            writer.WriteUInt32(0x3B);
            // writer.WriteBytes(new byte[] { 0x98, 0xC3, 0x71, 0x0F });

            byte[] reply = writer.ToArray();
            Client.Send(ref reply);
            Console.WriteLine("Sent Char Create 2");
            string fileLocation1 = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CharCreate0x3B20f3Server.txt");
            File.WriteAllText(fileLocation1, BitConverter.ToString(reply));
        }
    }
}
