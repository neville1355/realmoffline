﻿using RealmOffline.Core;
using RealmOffline.Core.Rooms;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class PlayerOrMobMovementRequest: BaseWorldRequest {
        public PlayerOrMobMovementRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            //Console.WriteLine("Start 41");
            int len = BitConverter.ToInt32(Reader.ReadBytes(4), 0);
            Reader.ReadBytes(4);
            uint ID = Reader.ReadUInt32();
            uint room = Reader.ReadUInt32();

            //  Console.WriteLine("Char id {0} from room {1}", ID, room);
            // -12
            byte[] rem = Reader.ReadBytes(len - 12);
            byte[] outBuffer = Tools.ReadUntil0xFF(rem);
            //log.LogMessage(string.Format("Payload {0}", BitConverter.ToString(outBuffer)), MessageType.Information);
            Reader.Close();
            /*
             * This packet is just a reply saying ok we got the data from you mr client
             * So we want this data, of where we think we are.
             */
            uint roomNumber = 0;
            int packId = -1;
            //ushort facing = 0;
            // ushort x = 0;
            // ushort y = 0;
            //  ushort charID = 0;

            byte[] payload = null;

            try {
                payload = Tools.ReadPacket41UntilFF(Packets, out packId, out roomNumber);
            }
            catch (Exception ex) { RealmPacketIO.log.WriteException(ex); }

            if (payload == null) { } // error out
            if (payload.Length == 0) { } // error out.
                                         // If the room isnt loaded, we cant set it
            bool roomExists = false;
            foreach (Room r in ScriptResolver.ImportedRooms) {
                if (r.RoomID == roomNumber) {
                    roomExists = true; break;
                }
            }

            if (roomExists) {
                if (Client.GameAccount.CurrentCharacter != null) {
                    if (Client.GameAccount.CurrentCharacter.Location == null) {
                        Client.GameAccount.CurrentCharacter.Location = new RoomLocation(Packets);
                    }
                    else {
                        Client.GameAccount.CurrentCharacter.Location.Repopulate(Packets);
                        Client.GameAccount.CurrentCharacter.Location.CurrentRoom.MoveEntity(Client.GameAccount.CurrentCharacter,
                            Client.GameAccount, rem);
                    }
                }
                PacketWriter w = new PacketWriter(0x19);
                w.WriteUInt32(Client.GameAccount.AccountId);
                w.WriteInt32(0);
                w.WriteInt32(0x29);
                w.WriteBytes(new byte[] { 0x7C, 0x4E, 0x0A, 0x08 });

                byte[] reply = w.ToArray();

                Client.Send(ref reply);
            }
            else Console.WriteLine("Logging in for first time");
        }
    }
}
