﻿using RealmOffline.Core;
using RealmOffline.Core.Rooms;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class SpellCastedRequest: BaseWorldRequest {
        public SpellCastedRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // Somewhere we gotta send a location to do this on.
            ///     Console.WriteLine("Start 80");
            // teleport too from town teleporter
            // does he just use a spell ?
            // Spell Cast Packet
            Reader.ReadBytes(4); // pck len
            Reader.ReadBytes(4); // id
            short spellID = BitConverter.ToInt16(Reader.ReadBytes(2), 0);
            uint caster = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            uint target = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            Reader.Close();
            // First get the caster, it can be a player or a npc or a mob in the room


            Room r = Client.GameAccount.CurrentCharacter.Location.CurrentRoom;
            Entity _caster = r.GetEntityInRoom(caster);
            Entity _target = r.GetEntityInRoom(target);


            string spellName = string.Empty;
            Enum _spell = SpellConvertor.FromShort(spellID);

            if (_spell is BaseSpell.Elementalism) spellName = ((BaseSpell.Elementalism)_spell).ToString();
            if (_spell is BaseSpell.Mysticsim) spellName = ((BaseSpell.Mysticsim)_spell).ToString();
            if (_spell is BaseSpell.Necromancy) spellName = ((BaseSpell.Necromancy)_spell).ToString();
            if (_spell is BaseSpell.Sorcery) spellName = ((BaseSpell.Sorcery)_spell).ToString();
            if (_spell is BaseSpell.Thaumaturgy) spellName = ((BaseSpell.Thaumaturgy)_spell).ToString();

            if (_spell != null) {
                //Console.WriteLine("Casting Spell ID:{0} ({1}) {2} is casting on {3}.",
                // string.Format("0x{0}",spellID.ToString("X2")), spellName, caster, target);
                byte[] tp = Packet.CastSpell(spellID, SpellConvertor.SpellEffect(_spell),
                    _caster, _target);
            }
            else Console.WriteLine("Spell {0} not found.", spellID);
            // First reply is 25 to give cursor back


            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0);
            w25.WriteUInt32(0x50);
            w25.WriteBytes(new byte[] { 0xD3, 0x3B, 0x09, 0x08 });
            byte[] reply = w25.ToArray();
            Client.Send(ref reply);


            PacketWriter w2B = new PacketWriter(0x2A);
            w2B.WriteUInt32(Client.GameAccount.AccountId);
            w2B.WriteUInt32(0x00);
            w2B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w2B.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w2B.WriteByte(0x1D);
            w2B.WriteUInt32(caster);
            w2B.WriteByte(0x2C);
            w2B.WriteUInt32(caster);
            if (caster != target)
                w2B.WriteString(string.Format("{0} says 'SpellName', pointing at the {1}.", Client.GameAccount.CurrentCharacter.Name, target));
            else w2B.WriteString(string.Format("{0} mumbles 'SpellName', under {1} breath.", Client.GameAccount.CurrentCharacter.Name, Client.GameAccount.CurrentCharacter.Gender == 0 ? "his" : "her"));
            w2B.WriteByte(0x25); // end spell text 2A on other example ?
            w2B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w2B.WriteInt32(-1); // how much the spell costs
            w2B.WriteByte(0x27);
            w2B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w2B.WriteShort(SpellConvertor.SpellEffect(_spell));
            //w2B.WriteShort(331); // the spell graphic
            w2B.WriteUInt32(target);
            w2B.WriteByte(0x0E);
            w2B.WriteUInt32(target);
            w2B.WriteBytes(new byte[] { 0x27, 0x00, 0x01, 0x2C }); // still not sure
            w2B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w2B.WriteString("The spell does all sorts of stuff to you or an item here.");

            // so now, what does it do ?

            w2B.WriteByte(0x1F); // end spell info
            w2B.WriteUInt32(caster);
            w2B.WriteByte(0xFF);
            byte[] p2B = w2B.ToArray();
            r.SendPacket(p2B);
            //Client.Send(ref p2B);



            // Now next packet to gimme my icon back
            PacketWriter w2C = new PacketWriter(0x2A);
            w2C.WriteUInt32(Client.GameAccount.AccountId);
            w2C.WriteUInt32(0x00);
            w2C.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w2C.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w2C.WriteBytes(new byte[] { 0x21, 0xFF });
            byte[] p2C = w2C.ToArray();
            Client.Send(ref p2C);
        }
    }
}
