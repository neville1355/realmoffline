﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class DyeRequest: BaseWorldRequest {
        public DyeRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Console.WriteLine("Dye packet ? {0}", BitConverter.ToString(Packets));
            PacketWriter w = new PacketWriter(0x19);
            w.WriteUInt32(0);
            w.WriteUInt32(0xA9);
            w.WriteBytes(new byte[] { 0x58, 0x23, 0x05, 0x08 });
            byte[] reply = w.ToArray();
            Client.Send(ref reply);
        }
    }
}
