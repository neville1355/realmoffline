﻿using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class ShopBuyRequest: BaseWorldRequest {
        public ShopBuyRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
        }
    }
}
