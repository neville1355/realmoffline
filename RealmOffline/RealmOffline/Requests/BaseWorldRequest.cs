﻿using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    public abstract class BaseWorldRequest : BaseRequest {
        protected WorldClient Client { get; }
        protected BinaryReader Reader { get; }

        public BaseWorldRequest(WorldClient client, byte[] packets) : base(packets) {
            Client = client;
            Reader = new BinaryReader(new MemoryStream(packets));
        }

        public override byte[] GetResponse() {
            //todo this maybe shouldn't be part of the base request after all...
            throw new NotImplementedException();
        }

        public abstract void SendResponse();
    }
}
