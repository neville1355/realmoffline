﻿using RealmOffline.Core;
using RealmOffline.Managers;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class ShowWhatsNewRequest: BaseWorldRequest {
        public ShowWhatsNewRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            PacketWriter w = new PacketWriter(0x72);
            w.WriteUInt32(Client.GameAccount.AccountId);
            w.WriteInt32(0);
            w.WriteBytes(new byte[] { 0x01, 0x7F, 0x03, 0x0A });

            List<WhatsNewMessage> all = MySqlManager.GetWhatsNew("*");

            StringBuilder b = new StringBuilder();
            if (all.Count == 0) {
                b.Append("|c86|Welcome to your new realm emulator server, log in game and type /wn add 5 anymessage to remove this default What's New!.\n");
            }
            else {
                foreach (WhatsNewMessage m in all)
                    b.Append(string.Format("|c{0}|{1} - written by {2} on {3}.\n", m.Color, m.Message, m.Author, m.TimeStamp));
            }

            w.WriteBytes(Encoding.ASCII.GetBytes(b.ToString()));
            while (w.Length % 4 != 0) {
                w.WriteByte(0x00);
            }
            byte[] reply = w.ToArray();
            Client.Send(ref reply);
        }
    }
}
