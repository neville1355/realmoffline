﻿using RealmOffline.Accounts;
using RealmOffline.Core;
using RealmOffline.Core.Items.Base;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class OpenDoorOrChestRequest: BaseWorldRequest {
        public OpenDoorOrChestRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            ////////////////////////////
            /////////////////////
            // FIXX all id uint now
            Character c = Client.GameAccount.CurrentCharacter;

            // We open doors
            Console.WriteLine("Open door packet 155: {0}", BitConverter.ToString(Packets));
            Reader.ReadBytes(8); // pck length and id
                                 // the door id ?
            uint itemid = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            // ushort itemid = BitConverter.ToUInt16(Reader.ReadBytes(2), 0);
            // Reader.ReadUInt16();
            Reader.ReadInt32();
            int passwordProtectedItem = Reader.ReadByte(); // is this byte FF ?
            string passWord = string.Empty;
            if (passwordProtectedItem == 255) {
                // We have a password
                // So we must have more bytes, lets check
                if (Reader.HasXBytesLeft(2)) {
                    // see if it ends up being a password length
                    ushort passwordLen = BitConverter.ToUInt16(Reader.ReadBytes(2), 0);
                    if (Reader.HasXBytesLeft(passwordLen)) {
                        passWord = Encoding.ASCII.GetString(Reader.ReadBytes(passwordLen));
                    }
                }
            }

            // Do we have a password ?
            if (passWord != string.Empty) {
                byte[] bad = { 0x23, 0x27, 0x00, 0x00 };
                //byte[] bad = { 0x21, 0x27, 0x00, 0x00 };
                // Ya
                Console.WriteLine("We are a password item, password is: {0}", passWord);

                if (passWord != "1234") {
                    PacketWriter w1A = new PacketWriter(0x1A);
                    w1A.WriteUInt32(Client.GameAccount.AccountId);
                    w1A.WriteUInt32(0x00);
                    w1A.WriteUInt32(0x9B);
                    w1A.WriteBytes(bad);
                    byte[] p1A = w1A.ToArray();
                    Client.Send(ref p1A);

                }
                else {
                    /// Fake password chk pass is 1234
                    // reply
                    PacketWriter w1 = new PacketWriter(0x19);
                    w1.WriteUInt32(Client.GameAccount.AccountId);
                    w1.WriteInt32(0x00);
                    w1.WriteInt32(0x9B);
                    w1.WriteBytes(new byte[] { 0xF8, 0x8A, 0xFF, 0xBF });
                    byte[] reply25 = w1.ToArray();
                    Client.Send(ref reply25);

                    PacketWriter w = new PacketWriter(42);
                    w.WriteUInt32(Client.GameAccount.AccountId);
                    w.WriteInt32(2);
                    w.WriteUInt32(c.GameID);
                    w.WriteUInt32(c.Location.CurrentRoom.RoomID);
                    w.WriteByte(0x0A);// room num
                                      //0xB8, 0x3F, 
                    w.WriteUInt32(c.GameID);
                    w.WriteUInt32(itemid);
                    //0xE6, 0x2B, // chest 
                    w.WriteBytes(new byte[] { 0x21, 0xFF, 0x00 });
                    //w.WriteBytes(new byte[] { 0x02, 0x00, 0x21, 0xFF, 0x00 });
                    byte[] reply42 = w.ToArray();
                    Client.Send(ref reply42);
                }
            }
            else {
                // Its just a item to open
                Console.WriteLine("We are a useable item like a door.");
                // reply

                // get the item, its a door
                foreach (BaseGameItem i in Client.GameAccount.CurrentCharacter.Location.CurrentRoom.Items) {
                    if (i is Door && i.GameID == itemid) {
                        Door d = i as Door;
                        if (d.IsLocked) {
                            // Door is locked bish !!! go away !! baiting !
                            PacketWriter w25 = new PacketWriter(0x1A);
                            w25.WriteUInt32(Client.GameAccount.AccountId);
                            w25.WriteUInt32(0x00);
                            w25.WriteUInt32(0x9B);
                            // byte[] say = GetByteArrayFromFileInBaseDir("notodoor.txt");
                            // w25.WriteBytes(say);
                            w25.WriteBytes(new byte[] { 0x14, 0x27, 0x00, 0x00 });
                            byte[] pno = w25.ToArray();
                            Client.Send(ref pno);
                        }
                        else {
                            // otherwize set open to the opposite of what it is.
                            if (d.IsOpen == false) d.IsOpen = true;
                            else d.IsOpen = false;

                            byte[] reply25 = Packet.ReplyWith25(Client.GameAccount.AccountId, 155,
                                new byte[] { 0x0A, 0xE3, 0x11, 0x08 });
                            Client.Send(ref reply25);
                            //roomwide
                            byte[] reply42 = Packet.OpenDoor42(Client.GameAccount, itemid);
                            //Client.Send(ref reply42);
                            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(reply42);
                        }
                    }
                }
            }
            Reader.Close();
        }
    }
}
