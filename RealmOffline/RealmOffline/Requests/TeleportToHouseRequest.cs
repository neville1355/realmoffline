﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class TeleportToHouseRequest: BaseWorldRequest {
        public TeleportToHouseRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            //     Console.WriteLine("Start 0x5E");
            Reader.ReadBytes(8);
            short len = Reader.ReadInt16();
            string name = Encoding.ASCII.GetString(Reader.ReadBytes(len));
            Reader.Close();

            Console.WriteLine("Tele to home ? {0}", name);

            // Well, we need to get the players house here.


            PacketWriter w26A = new PacketWriter(0x1A);
            w26A.WriteUInt32(Client.GameAccount.AccountId);
            w26A.WriteUInt32(0x00);
            w26A.WriteUInt32(0x5E);
            w26A.WriteUInt32(10013);
            byte[] p26A = w26A.ToArray();
            Client.Send(ref p26A);


            PacketWriter w2A = new PacketWriter(0x2A);
            w2A.WriteUInt32(Client.GameAccount.AccountId);
            w2A.WriteUInt32(0x00);
            w2A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            // current room we are in
            w2A.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w2A.WriteByte(0x2A);
            w2A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w2A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w2A.WriteUInt32(5046); // the room to goto
            w2A.WriteByte(0xFF);
            byte[] p2A = w2A.ToArray();
            Client.Send(ref p2A);
        }
    }
}
