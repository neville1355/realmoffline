﻿using RealmOffline.Accounts;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class UseItemRequest: BaseWorldRequest {
        public UseItemRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // Item Use
            Console.WriteLine("Received a item use packet 161: {0}", BitConverter.ToString(Packets));
            Reader.ReadBytes(4);
            Reader.ReadBytes(4);
            // this is the item they used, or more specifically the slot in inventory
            // thats being accessed because we should already know what item is there.
            ushort itemSlot = BitConverter.ToUInt16(Reader.ReadBytes(2), 0);
            Reader.Close();
            Character c = Client.GameAccount.CurrentCharacter;

            byte[] pck25 = RealmPacketIO.UseItemPacket25(Client.GameAccount.AccountId);
            Client.Send(ref pck25);
            byte[] pck42 = RealmPacketIO.UseItemPacket42(Client.GameAccount, itemSlot);
            Client.Send(ref pck42);
        }
    }
}
