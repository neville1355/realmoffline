﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class DropGoldOrManaRequest: BaseWorldRequest {
        public DropGoldOrManaRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // drop gold
            //    Console.WriteLine("Pak {0}", BitConverter.ToString(pak));
            Reader.ReadBytes(8);
            int amt = Reader.ReadInt32();
            byte type = Reader.ReadByte();
            Reader.Close();
            Console.WriteLine("{0} wants to drop {1} {2}.", Client.GameAccount.CurrentCharacter.Name, amt, type == 0 ? "Gold" : "Mana");

            uint id = ServerGlobals.GetNextAvailableID();

            PacketWriter w42A = new PacketWriter(0x2A);
            w42A.WriteUInt32(Client.GameAccount.AccountId);
            w42A.WriteUInt32(0x00);
            w42A.WriteUInt32(id);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42A.WriteByte(0x3F);
            w42A.WriteShort(0x22);
            w42A.WriteUInt32(0x00);
            w42A.WriteByte(0x01);
            if (type == 0)
                w42A.WriteShort(0x02);
            else
                w42A.WriteShort(0x03);
            w42A.WriteUInt32(id);
            w42A.WriteBytes(new byte[3]);
            w42A.WriteByte(0xFF);
            w42A.WriteByte(0x02);
            w42A.WriteInt32(amt);
            w42A.WriteByte(0x02);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42A.WriteShort(0xFD);
            w42A.WriteShort(0xD4);
            w42A.WriteUInt32(0x00);
            w42A.WriteShort(0x00);
            w42A.WriteShort(0x01);
            w42A.WriteByte(0xFF);
            byte[] pw42A = w42A.ToArray();
            Client.Send(ref pw42A);


            // now 25
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0x00);
            w25.WriteUInt32(0x4C);
            w25.WriteUInt32(0x00);
            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);


            // Now add the actual item to room
            PacketWriter w42B = new PacketWriter(0x2A);
            w42B.WriteUInt32(Client.GameAccount.AccountId);
            w42B.WriteUInt32(0x00);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42B.WriteByte(0x19);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42B.WriteInt32(amt);
            w42B.WriteUInt32(id);
            w42B.WriteByte(0xFF);
            byte[] pw42B = w42B.ToArray();
            Client.Send(ref pw42B);

            // Give icon back
            PacketWriter w42C = new PacketWriter(0x2A);
            w42C.WriteUInt32(Client.GameAccount.AccountId);
            w42C.WriteUInt32(0x00);
            w42C.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42C.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42C.WriteByte(0x21);
            w42C.WriteByte(0xFF);
            byte[] p42C = w42C.ToArray();
            Client.Send(ref p42C);
        }
    }
}
