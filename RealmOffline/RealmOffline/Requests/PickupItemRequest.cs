﻿using RealmOffline.Accounts;
using RealmOffline.Core.Items.Base;
using RealmOffline.Managers;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class PickupItemRequest: BaseWorldRequest {
        public PickupItemRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Console.WriteLine("Packet 150 Pickup Item: {0}", BitConverter.ToString(Packets));
            Character c = Client.GameAccount.CurrentCharacter;
            Reader.ReadBytes(8);
            uint itemNum = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            Reader.Close();
            // So it can be in room, or in a backpack
            BaseGameItem i = null;
            foreach (BaseGameItem item in Client.GameAccount.CurrentCharacter.Location.CurrentRoom.Items) {
                if (item.GameID == itemNum) i = item;
            }
            // ok check our inventory if still null
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteInt32(0);
            w25.WriteInt32(0x96); // replying to pack 96 pickup
            w25.WriteInt32(0);
            byte[] p25 = w25.ToArray();

            if (i == null) {
                foreach (BaseGameItem item in Client.GameAccount.CurrentCharacter.Inventory) {
                    if (item.GameID == itemNum) i = item;
                }

            }
            if (i == null) {
                Console.WriteLine("Cant find item {0}", itemNum);
                Client.Send(ref p25);
                return;
            }


            Client.Send(ref p25);

            // Cannot pick it up until i know exactly how it is formatted
            PacketWriter w42 = new PacketWriter(0x2A);
            w42.WriteUInt32(Client.GameAccount.AccountId);
            w42.WriteInt32(0);
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42.WriteByte(0x05);
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42.WriteUInt32(itemNum);
            w42.WriteByte(0x21);
            w42.WriteByte(0xFF);
            byte[] qr = w42.ToArray();
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(qr, false);
            // try to remove it from room if able
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RemoveEntity(i, Client.GameAccount);
            // add it to current inventory list
            Client.GameAccount.CurrentCharacter.AddInventoryItem(i);
            i.RemoveFromRoom();

            //TODO: Should we save the item now ?
            // if not rollbacks will occur if client crashes or handles.

            bool save = MySqlManager.SaveItem(Client.GameAccount.CurrentCharacter.SqlCharId, i);
            //Client.Send(ref qr);
            Console.WriteLine("Sent 150");
        }
    }
}
