﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class MoveItemToBagRequest: BaseWorldRequest {
        public MoveItemToBagRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Console.WriteLine("Put item in container {0}", BitConverter.ToString(Packets));
            Reader.ReadBytes(8);
            uint container = Reader.ReadUInt32();
            uint item = Reader.ReadUInt32();
            Reader.Close();
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0x00);
            w25.WriteUInt32(0x98);
            w25.WriteBytes(new byte[] { 0x65, 0x25, 0x05, 0x08 });
            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);

            PacketWriter w42A = new PacketWriter(0x2A);
            w42A.WriteUInt32(Client.GameAccount.AccountId);
            w42A.WriteUInt32(0x00);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42A.WriteByte(0x09);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            // item thats going in
            w42A.WriteUInt32(item);
            // into this container
            w42A.WriteUInt32(container);
            w42A.WriteByte(0x21);
            w42A.WriteByte(0xFF);
            byte[] p42A = w42A.ToArray();
            Client.Send(ref p42A);
        }
    }
}
