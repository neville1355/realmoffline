﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class PingRequest: BaseWorldRequest {
        public PingRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            PacketWriter w = new PacketWriter(25);
            w.WriteUInt32(Client.GameAccount.AccountId);
            w.WriteInt32(0);
            w.WriteShort(0x11);
            w.WriteBytes(new byte[] { 0x8C, 0x72, 0x11, 0x08 });
            byte[] reply = w.ToArray();

            Client.Send(ref reply);
        }
    }
}
