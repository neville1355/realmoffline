﻿using RealmOffline.Accounts;
using RealmOffline.Core;
using RealmOffline.Core.Rooms;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class LogInToLastRoomRequest: BaseWorldRequest {
        public LogInToLastRoomRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Console.WriteLine(BitConverter.ToString(Packets));
            // Console.WriteLine("Start 0x5C");
            /* No need to read this packet, it doesnt contain
             * Any specfic info, just get the charname
             * and find out the last room they logged out
             * and the last locs, then send that.
             */
            // First time a room number is referenced,
            Character c = Client.GameAccount.CurrentCharacter;

            PacketWriter w = new PacketWriter(0x19);
            w.WriteUInt32(Client.GameAccount.AccountId);
            w.WriteInt32(0);
            w.WriteInt32(0x5C);
            w.WriteInt32(0);
            // We just logged on, get the last room we were in
            // and the last x,y,facing and send it

            Client.GameAccount.CurrentCharacter.Location = new RoomLocation(5043, 150, 150, 2);
            w.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.RoomNumber); // Send the room
                                                                                 // Set chars room 
            w.WriteUShort(Client.GameAccount.CurrentCharacter.Location.X); // X
            w.WriteUShort(Client.GameAccount.CurrentCharacter.Location.Y); // Y
            w.WriteUShort(Client.GameAccount.CurrentCharacter.Location.Facing); // N/S/W/E 3/2/1/0
            c.Location = new RoomLocation(5043, 150, 150, 2);
            Room r = ScriptResolver.GetRoom(5043);
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom = r;
            w.WriteBytes(new byte[] { 0xC3, 0x28 });
            byte[] room = w.ToArray();
            Client.Send(ref room);
            // Send empty loc packet

            PacketWriter l = new PacketWriter(0x29);
            l.WriteUInt32(Client.GameAccount.AccountId);
            l.WriteUInt32(0x00);
            l.WriteUInt32(0x29);
            l.WriteUInt32(0x01);
            byte[] loc = l.ToArray();
            Client.Send(ref loc);
        }
    }
}
