﻿using RealmOffline.Base;
using RealmOffline.Core;
using RealmOffline.Managers;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class DebuggingInformationRequest: BaseWorldRequest {
        public DebuggingInformationRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Account a = Client.GameAccount;
            DebugInfo i = new DebugInfo(a, Packets);

            if (!MySqlManager.SaveDebugInfo(i)) Console.WriteLine("Error Saving Debugging info to Mysql!!");

            PacketWriter w = new PacketWriter(0x19);
            w.WriteInt32(-1);
            w.WriteInt32(0);
            w.WriteInt32(0x54);
            byte[] f = w.ToArray();
            Client.Send(ref f);
            Console.WriteLine(Encoding.ASCII.GetString(Packets));
        }
    }
}
