﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class ShowShopkeeperScreenRequest: BaseWorldRequest {
        public ShowShopkeeperScreenRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            byte[] t = Packet.DD(Client.GameAccount);
            Client.Send(ref t);
        }
    }
}
