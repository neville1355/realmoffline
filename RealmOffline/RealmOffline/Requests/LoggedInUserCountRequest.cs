﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class LoggedInUserCountRequest: BaseWorldRequest {
        public LoggedInUserCountRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // Router wants to know how many players are on
            PacketWriter w = new PacketWriter(0xE6);
            w.WriteString(ServerGlobals.LoggedInAccounts.Count.ToString());
            byte[] reply = w.ToArray();
            //Console.WriteLine("Router from {0} requested player count.", cli.TcpSocket.LocalEndPoint);
            Client.Send(ref reply);
        }
    }
}
