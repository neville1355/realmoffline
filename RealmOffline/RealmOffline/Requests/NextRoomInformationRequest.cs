﻿using RealmOffline.Core;
using RealmOffline.Core.Rooms;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class NextRoomInformationRequest : BaseWorldRequest {
        public NextRoomInformationRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            /// AHHHHHHH this is why, it asks for a room here
            Reader.ReadBytes(8);
            // what room do we want ?
            uint room1 = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            ushort dunno = BitConverter.ToUInt16(Reader.ReadBytes(2), 0);
            uint room2 = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            Reader.Close();

            // Here we go
            byte[] sendNewRoom = null;

            foreach (Room r in ScriptResolver.ImportedRooms) {
                if (room1 == r.RoomID) {
                    // Console.WriteLine("Room {0} Found.", room1);
                    // If location is null, we just logged on
                    if (Client.GameAccount.CurrentCharacter.Location == null)
                        Client.GameAccount.CurrentCharacter.Location = new RoomLocation(r.RoomID, 150, 150, 2);
                    r.AddEntity(Client.GameAccount.CurrentCharacter, Client.GameAccount);
                    Client.GameAccount.CurrentCharacter.Location.CurrentRoom = r;

                    sendNewRoom = r.GetRoomPacket(Client.GameAccount.AccountId);
                }
            }

            // just in case the room doesnt exist, we send them to a default room
            // this will be thier home eventually
            if (sendNewRoom == null) {
                Room rfault = ScriptResolver.ImportedRooms[0];
                // If location is null, we just logged on

                if (Client.GameAccount.CurrentCharacter.Location == null)
                    Client.GameAccount.CurrentCharacter.Location = new RoomLocation(rfault.RoomID, 150, 150, 2);
                rfault.AddEntity(Client.GameAccount.CurrentCharacter, Client.GameAccount);
                Client.GameAccount.CurrentCharacter.Location.CurrentRoom = rfault;

                sendNewRoom = ScriptResolver.ImportedRooms[0].GetRoomPacket(Client.GameAccount.AccountId);
            }

            /// This all needs moved, we only send this on connect.

            // Now send uptime n shit
            byte[] connected = RealmPacketIO.ServerMessageType2(ServerGlobals.GetCurrentNumberOfPlayers);
            Client.Send(ref connected);
            //  Console.WriteLine("Sent con");
            // How long has server been up ?
            byte[] uptime = RealmPacketIO.ServerMessageType2(ServerGlobals.GetCurrentUpTime);
            Client.Send(ref uptime);
            if (sendNewRoom == null) Console.WriteLine("New room is null");
            //Room Last
            Client.Send(ref sendNewRoom);
            Console.WriteLine("Sent up");
        }
    }
}
