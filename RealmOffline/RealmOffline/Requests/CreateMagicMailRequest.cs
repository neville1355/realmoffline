﻿using RealmOffline.Core;
using RealmOffline.Managers;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class CreateMagicMailRequest: BaseWorldRequest {
        public CreateMagicMailRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            //   Console.WriteLine("Start 0x8A");
            // Read the mail
            Reader.ReadBytes(4); //len
            Reader.ReadBytes(4); // id
            Reader.ReadBytes(4); // act id
            Reader.ReadByte(); // spacer
            int subLen = Reader.ReadInt16();
            string subject = Encoding.ASCII.GetString(Reader.ReadBytes(subLen));
            int bodyLen = Reader.ReadInt16();
            string body = Encoding.ASCII.GetString(Reader.ReadBytes(bodyLen));
            int fromLen = Reader.ReadUInt16();
            string to = Encoding.ASCII.GetString(Reader.ReadBytes(fromLen));
            Reader.Close();

            // Parse to, if it has Name;name;name;name;
            // then try to send to all


            if (Client.GameAccount.CurrentCharacter.GameMail == null)
                Client.GameAccount.CurrentCharacter.GameMail = new MagicMail(Client.GameAccount);
            // Get the player we want to send too
            uint sqlid = 0;
            Mail m = null;
            if (MySqlManager.TryFindCharacter(to, out sqlid)) {
                // Create the Mail
                m = new Mail();
                m.Body = body;
                m.From = Client.GameAccount.CurrentCharacter.Name;
                m.Subject = subject;
                m.TimeStamp = (int)MagicMail.DateTimeToUnixTimestamp(DateTime.Now);
                m.MailID = Client.GameAccount.CurrentCharacter.GameMail.NextMailID;
                //    Console.WriteLine("Next Mail Id is {0}", m.MailID);
                //Done.
                if (MySqlManager.WriteMail(m, sqlid)) {
                    PacketWriter w = new PacketWriter(0x19);
                    w.WriteUInt32(Client.GameAccount.AccountId);
                    w.WriteInt32(0);
                    w.WriteShort(0x8A);
                    w.WriteInt32(0);
                    w.WriteInt32(m.MailID); // the mails ID

                    byte[] reply = w.ToArray();
                    Client.Send(ref reply);
                }
                else {
                    //todo: move this back to using RLog
                    Console.WriteLine(string.Format("Unable to write Magic Mail from {0} to {1}, to the database.", Client.GameAccount.CurrentCharacter.Name, to));
                }
            }
            else {
                PacketWriter w = new PacketWriter(0x1A);
                w.WriteUInt32(Client.GameAccount.AccountId);
                w.WriteInt32(0);
                w.WriteInt32(0x87);
                w.WriteBytes(new byte[] { 0x14, 0x27 });
                w.WriteShort(0);
                byte[] reply = w.ToArray();

                PacketWriter w1 = new PacketWriter(0x1A);
                w1.WriteUInt32(Client.GameAccount.AccountId);
                w1.WriteInt32(0);
                w1.WriteInt32(0x8A);
                w1.WriteInt32(0);
                byte[] msg = Encoding.ASCII.GetBytes(string.Format("Player {0} not found, unable to send magic mail.", to));
                w1.WriteShort((short)msg.Length);
                w1.WriteBytes(msg);
                w1.WriteShort(0);
                w1.WriteBytes(new byte[] { 0x00, 0xA8 });
                byte[] reply2 = w1.ToArray();

                Client.Send(ref reply);
                Client.Send(ref reply2);
            }
            //Console.WriteLine("Sent 0x8A");
        }
    }
}
