﻿using RealmOffline.Accounts;
using RealmOffline.Core;
using RealmOffline.Core.Items.Base;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class InspectItemRequest: BaseWorldRequest {
        public InspectItemRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            //   Console.WriteLine("Start 57");
            // Look at item
            //  Console.WriteLine("Packet 57 answered. {0}", BitConverter.ToString(pak));
            //     Console.WriteLine("Packet 57: {0}", BitConverter.ToString(pak));
            Reader.ReadBytes(4); // pak length
            Reader.ReadBytes(4); // Id 57
            uint slotId = Reader.ReadUInt32();
            // what are we doing with it ?
            //byte action = Reader.ReadByte();
            Reader.Close();
            // Console.WriteLine("We want to look at item or mob id {0}", slotId);

            // Needs to look everywhere, inventory first
            BaseGameItem i = null;// MySqlManager.GetItem(slotId);
            foreach (BaseGameItem item in Client.GameAccount.CurrentCharacter.Inventory) {
                if (item.GameID == slotId) i = item;
            }
            // Does i still = null ?
            if (i == null) {
                // Then look through the room
                foreach (BaseGameItem item in Client.GameAccount.CurrentCharacter.Location.CurrentRoom.Items) {
                    if (item.GameID == slotId) i = item;
                }

            }
            if (i != null && i.GameID != 0) {
                PacketWriter w = new PacketWriter(0x19);
                w.WriteUInt32(Client.GameAccount.AccountId);
                w.WriteUInt32(0x00);
                w.WriteUInt32(0x39);
                w.WriteUInt32(0);
                string name = i.Name;
                if (name == string.Empty) name = "Default Name.";
                w.WriteString(string.Format(
                    "You see a {0} with a Color of {1}, it {2} equipped.\n" +
                    "It's Graphic id is {3}, SqlID is {4}, The SqlId of it's current owner is {5}.\n" +
                    "The type of the item is {6}, and it's look at is {7}, with a Current Game ID of {8}.",
                    i.Name, i.Color, i.Equipped ? "is" : "is not",
                    i.GraphicID, i.SqlID, i.SqlOwnerID,
                    i.Type, i.LookAt, i.GameID));
                byte[] r = w.ToArray();
                Client.Send(ref r);
            }
            else {
                // we might be a player in a costume.
                Character c = Client.GameAccount.CurrentCharacter.Location.CurrentRoom.GetCharacterInRoom(slotId);//MySqlManager.GetCurrentCharacter(slotId);
                if (c != null) {
                    PacketWriter w = new PacketWriter(0x19);
                    w.WriteUInt32(Client.GameAccount.AccountId);
                    w.WriteUInt32(0x00);
                    w.WriteUInt32(0x39);
                    w.WriteUInt32(0);
                    string name = c.Name;
                    w.WriteString(string.Format(
                        "Player {0} is currently in a costume.", c.Name));
                    byte[] r = w.ToArray();
                    Client.Send(ref r);
                }
                else if (c == null) {
                    Mobile nfound = null;
                    // Maybe we are a npc ?
                    foreach (Mobile m in Client.GameAccount.CurrentCharacter.Location.CurrentRoom.Npcs) {
                        if (m.GameID == slotId) {
                            nfound = m;
                            break;
                        }
                    }
                    if (nfound != null) {
                        PacketWriter w = new PacketWriter(0x19);
                        w.WriteUInt32(Client.GameAccount.AccountId);
                        w.WriteUInt32(0x00);
                        w.WriteUInt32(0x39);
                        w.WriteUInt32(0);
                        w.WriteString(string.Format(
                            "You see a NPC {0}, {1} GameID is {2}.", nfound.Name, nfound.Gender == 0 ? "his" : "her", nfound.GameID));
                        byte[] r = w.ToArray();
                        Client.Send(ref r);
                    }
                    else Console.WriteLine("No found npc {0}", slotId);
                }
                else {
                    // Look at a item
                    byte[] reply = { 0xB0, 0x00, 0x00, 0x00,
                                         0x19, 0x00, 0x00, 0x00,
                                         0xD4, 0x9E,
                                         0x02, 0x00,
                                         0x00, 0x00, 0x00, 0x00,
                                         0x39, 0x00, 0x00, 0x00,
                                         0x00, 0x00, 0x00, 0x00, 
                                         // Description of the item
                                         0x96, 0x00,
                                         0x59, 0x6F, 0x75, 0x20, 0x73, 0x65, 0x65, 0x20, 0x61, 0x20, 0x53,
                                         0x6B, 0x69, 0x6C, 0x6C, 0x65, 0x64, 0x20, 0x50, 0x6C, 0x61, 0x79,
                                         0x65, 0x72, 0x27, 0x73, 0x20, 0x42, 0x61, 0x6C, 0x64, 0x72, 0x69,
                                         0x63, 0x2E, 0x20, 0x54, 0x68, 0x69, 0x73, 0x20, 0x69, 0x74, 0x65,
                                         0x6D, 0x20, 0x68, 0x61, 0x73, 0x20, 0x61, 0x6E, 0x20, 0x61, 0x72,
                                         0x6D, 0x6F, 0x72, 0x20, 0x72, 0x61, 0x74, 0x69, 0x6E, 0x67, 0x20,
                                         0x6F, 0x66, 0x20, 0x31, 0x30, 0x20, 0x70, 0x65, 0x72, 0x63, 0x65,
                                         0x6E, 0x74, 0x2E, 0x20, 0x20, 0x54, 0x68, 0x69, 0x73, 0x20, 0x69,
                                         0x74, 0x65, 0x6D, 0x20, 0x77, 0x65, 0x69, 0x67, 0x68, 0x73, 0x20,
                                         0x30, 0x2E, 0x35, 0x20, 0x73, 0x74, 0x6F, 0x6E, 0x65, 0x73, 0x2E,
                                         0x20, 0x20, 0x54, 0x68, 0x69, 0x73, 0x20, 0x69, 0x73, 0x20, 0x69,
                                         0x6E, 0x20, 0x65, 0x78, 0x63, 0x65, 0x6C, 0x6C, 0x65, 0x6E, 0x74,
                                         0x20, 0x63, 0x6F, 0x6E, 0x64, 0x69, 0x74, 0x69, 0x6F, 0x6E, 0x20,
                                         0x28, 0x31, 0x30, 0x30, 0x25, 0x29, 0x2E,

                                         0x00, 0x00, 0x02, 0x00,

                                         0x00, 0x00, 0x00, 0x00 };
                    // byte 8 and 9 are the items slot id, this needs to change to reply to what slot char is looking at atm
                    //  reply[8] = slotId[0];
                    //  reply[9] = slotId[1];
                    // reply[10] = slotId[2];
                    // reply[11] = slotId[3];
                    Client.Send(ref reply);
                }
            }
        }
    }
}
