﻿using RealmOffline.Base;
using RealmOffline.Managers;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class AuthenticateUserRequest: BaseWorldRequest {
        public AuthenticateUserRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            PacketReader r = new PacketReader(Packets);
            r.ReadBytes(8);
            string uName = r.ReadString16().Trim();
            string uPpass = r.ReadString16().Trim();
            r.Close();

            //ushort userNamLen = BitConverter.ToUInt16(reader.ReadBytes(2), 0);
            //byte[] usernameBytes = reader.ReadBytes(userNamLen);
            //ushort passLen = BitConverter.ToUInt16(reader.ReadBytes(2), 0);
            //byte[] pass = reader.ReadBytes(passLen);
            //reader.Close();

            bool valid = false;
            //string uName = user;
            //string uPpass = pass;

            // Good reply 
            // Send packet 98 (We got your login Packet reply)
            PacketWriter recv = new PacketWriter(0x62);
            recv.WriteInt32(0);
            recv.WriteInt32(0);
            recv.WriteInt32(0); // Changing number on live no clue why
            byte[] recieved = recv.ToArray();
            Client.Send(ref recieved);

            // Send packet 14

            byte[] good = null;
            byte[] bad = Packet.BadLogon("Incorrect Username or password.");



            // Ok we are valid, are we logged on already ?
            uint uid = MySqlManager.GetUserID(uName);

            valid = MySqlManager.CheckLogin(uName, uPpass);
            if (!ServerGlobals.IsAccountInUse(uid)) {
                if (valid) {
                    // get and set the account id

                    if (uid > 0) {
                        uint sid = ServerGlobals.GetNextAvailableID();
                        Client.SetGameAccount(new Account(Client, sid, uid));
                        if (Client.GameAccount != null) {
                            Client.GameAccount.Username = uName;
                            // Logon
                            ServerGlobals.LoggedInAccounts.QueueItem(Client.GameAccount);
                            good = Packet.GoodLogon(Client.GameAccount);
                            Client.Send(ref good);
                        }
                        else
                            Client.Send(ref bad);
                    }
                }
                else {
                    if (ServerGlobals.AutoAccountCreation && !string.IsNullOrEmpty(uName)) {
                        string error = string.Empty;
                        if (MySqlManager.CreateUserAccount(uName, uPpass, "AutoCreate", "Autocreate", 5, out error)) {
                            uid = MySqlManager.GetUserID(uName);
                            if (uid > 0) {
                                uint sid = ServerGlobals.GetNextAvailableID();
                                byte[] acctID = BitConverter.GetBytes(sid);
                                Client.SetGameAccount(new Account(Client, sid, uid));
                                if (Client.GameAccount != null) {
                                    Client.GameAccount.Username = uName;
                                    // Logon
                                    ServerGlobals.LoggedInAccounts.QueueItem(Client.GameAccount);
                                    good = Packet.GoodLogon(Client.GameAccount);
                                    Client.Send(ref good);
                                }
                                else
                                    Client.Send(ref bad);
                            }
                            else
                                Client.Send(ref bad);
                        }
                        else
                            Client.Send(ref bad);
                    }
                    else
                        Client.Send(ref bad);
                }
            }
            else {
                // Account is in use
                byte[] invalid = Packet.BadLogon("This account is currently logged on, if you have logged off this account, please check back shortly.");
                Client.Send(ref invalid);
            }
        }
    }
}
