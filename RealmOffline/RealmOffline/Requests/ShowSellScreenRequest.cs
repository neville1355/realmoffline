﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class ShowSellScreenRequest: BaseWorldRequest {
        public ShowSellScreenRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Reader.ReadBytes(8);
            uint shopkeeperid = Reader.ReadUInt32();
            Reader.Close();
            Console.WriteLine("Using shopkeeper {0}", BitConverter.ToString(Packets));

            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0x00);

            w25.WriteInt32(-1);
            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);
        }
    }
}
