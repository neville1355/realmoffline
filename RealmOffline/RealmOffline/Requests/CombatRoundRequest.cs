﻿using RealmOffline.Core;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class CombatRoundRequest: BaseWorldRequest {
        public CombatRoundRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Reader.ReadBytes(8);
            byte action = Reader.ReadByte();

            Console.WriteLine("Combat round {0} with {1} mobs in combat.",
                BitConverter.ToString(Packets), Client.GameAccount.CurrentCharacter.Cloud.Combatants.Count);
            switch (action) {
                case 0:
                    Console.WriteLine("Wants to move");
                    break;
                case 1:
                    Console.WriteLine("Attacked");
                    break;
                case 2:
                    Console.WriteLine("Guarded");
                    break;
                case 4:
                    Console.WriteLine("Flee");
                    break;
                case 5: {
                        short spellID = Reader.ReadInt16();
                        uint caster = Reader.ReadUInt32();
                        uint target = Reader.ReadUInt32();
                        string spellName = string.Empty;
                        Enum _spell = SpellConvertor.FromShort(spellID);

                        if (_spell is BaseSpell.Elementalism) spellName = ((BaseSpell.Elementalism)_spell).ToString();
                        if (_spell is BaseSpell.Mysticsim) spellName = ((BaseSpell.Mysticsim)_spell).ToString();
                        if (_spell is BaseSpell.Necromancy) spellName = ((BaseSpell.Necromancy)_spell).ToString();
                        if (_spell is BaseSpell.Sorcery) spellName = ((BaseSpell.Sorcery)_spell).ToString();
                        if (_spell is BaseSpell.Thaumaturgy) spellName = ((BaseSpell.Thaumaturgy)_spell).ToString();

                        // if the spell is a aoe, loc info follows target (target is same as caster for aoe)
                        byte[] r = Packet.ChatPacket(1, 1, string.Format("Casting Spell {0} while in Combat!", spellName.ToUpper()), "Debug");
                        Client.Send(ref r);
                    }
                    break;
                case 7:
                    Console.WriteLine("Wants to charge");
                    break;
                default:
                    Console.WriteLine("Unk Action {0}", action);
                    break;
            }
            Reader.Close();
            PacketWriter w42 = new PacketWriter(0x2A);
            w42.WriteUInt32(Client.GameAccount.AccountId);
            w42.WriteUInt32(0x00);

            // Start with the cloud id
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID);
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42.WriteByte(0x12);
            // get the mob to move this round
            Mobile m = Client.GameAccount.CurrentCharacter.Cloud.Combatants[0];
            w42.WriteUInt32(m.GameID);
            // w42.WriteBytes(new byte[] { 0x14, 0x06, 0x12 });
            // w42.WriteUInt32(Client.GameAccount.CurrentCharacter.CharId);
            // w42.WriteBytes(new byte[] { 0x04, 0x06, 0x12 });
            //w42.WriteUInt32(m.GameID);
            // w42.WriteBytes(new byte[] { 0x11, 0x06, 0x12 });
            // w42.WriteUInt32(Client.GameAccount.CurrentCharacter.CharId);
            // w42.WriteBytes(new byte[] { 0x07, 0x06, 0x12 });
            // w42.WriteUInt32(m.GameID);
            // w42.WriteBytes(new byte[] { 0x0E, 0x06, 0x12 });
            //  w42.WriteUInt32(Client.GameAccount.CurrentCharacter.CharId);
            // w42.WriteBytes(new byte[] { 0x0A, 0x06, 0x12 });
            // w42.WriteUInt32(m.GameID);
            w42.WriteBytes(new byte[] { 0x07, 0x06, 0x11 });

            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42.WriteUInt32(m.GameID);
            w42.WriteBytes(new byte[] { 0x04, 0x1E });
            w42.WriteUInt32(m.GameID);
            // w42.WriteInt32(-5000); // how much u hit for
            Console.WriteLine("New hit");
            //w42.WriteInt32(0x00);
            int idmg = int.MinValue;
            w42.WriteInt32(idmg); // if we change this.
                                  //w42.WriteBytes(new byte[] { 0xC2, 0xFE, 0xFF, 0xFF});
            w42.WriteByte(0x00);
            w42.WriteByte(0x24);
            w42.WriteUInt32(m.GameID);
            w42.WriteByte(0x06);
            w42.WriteUInt32(m.GameID);
            w42.WriteBytes(new byte[] { 0xAC, 0x91, 0x00, 0x00, 0x2D });
            w42.WriteUInt32(m.GameID);
            string dmg = idmg.ToString();
            w42.WriteString(dmg); // the damage done in text
            w42.WriteByte(0x2C);
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42.WriteString("|c21|You totally |c15|demolish the fuck |c60|out of what you just bitch |c9|smacked, |c82|Man that |c67|totally |c38|freaked |c63|out |c109|out!");
            w42.WriteByte(0x23);
            w42.WriteUInt32(m.GameID);
            w42.WriteBytes(new byte[] { 0x0B, 0x02, 0x27, 0x01 }); // nothing i can see
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42.WriteByte(0x3C);
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID);
            w42.WriteByte(0x35); // crash
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID);
            w42.WriteByte(0x17); // crash
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42.WriteInt32(int.MaxValue); // exp gain or loss
            w42.WriteByte(0xFF);
            byte[] p42 = w42.ToArray();
            Client.Send(ref p42);
        }
    }
}
