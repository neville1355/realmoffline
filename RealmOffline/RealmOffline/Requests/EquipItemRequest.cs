﻿using RealmOffline.Accounts;
using RealmOffline.Core.Items.Base;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class EquipItemRequest: BaseWorldRequest {
        public EquipItemRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // This equips a item
            //      Console.WriteLine("Packet 153 Equip Item: {0}", BitConverter.ToString(pak));
            Reader.ReadBytes(4); // len
            Reader.ReadBytes(4); //id
                                 // byte[] bslotId = Reader.ReadBytes(4);
            uint slotId = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            Character c = Client.GameAccount.CurrentCharacter;
            // send 25
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0);
            w25.WriteUInt32(0x99);
            w25.WriteBytes(new byte[] { 0x65, 0x25, 0x05, 0x08 });
            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);

            BaseGameItem b = Client.GameAccount.CurrentCharacter.GetInventoryItem(slotId);

            byte[] r = Packet.ChatPacket(1, 1,
                  string.Format("You equiped {0}", b.EquipableSlot), "Equip");
            Client.Send(ref r);

            if (!Client.GameAccount.CurrentCharacter.IsMob) {

                // You have a different packet than room for this
                PacketWriter w42 = new PacketWriter(0x2A);
                w42.WriteUInt32(Client.GameAccount.AccountId);
                w42.WriteInt32(0);
                w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
                w42.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
                w42.WriteByte(0x07);
                w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
                w42.WriteUInt32(slotId);
                w42.WriteByte(0x2F);
                w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
                w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);

                // These are item stats  ? figure them out
                //w42.WriteBytes(new byte[] { 0x2D, 0x00, 0x00, 0x00, 0x21, 0xFF });
                // sets max hp, use the chars current if no change
                w42.WriteUInt32((uint)Client.GameAccount.CurrentCharacter.TotalHP);

                w42.WriteBytes(new byte[] { 0x21, 0xFF });
                //w42.WriteBytes(new byte[] { 0x00, 0x12 }); // not sure
                //w42.WriteBytes(new byte[] { 0xA1, 0xF3 });
                w42.WriteBytes(new byte[4]);
                byte[] p42 = w42.ToArray();
                Client.Send(ref p42);
                // Now send to room
                Client.GameAccount.CurrentCharacter.Location.CurrentRoom.EquipItemInRoom(Client.GameAccount, slotId);
            }
            else {
                byte[] reply = Packet.ChatPacket(1, 1, "You have no need for weapons or armor in that form.", "Info");
                Client.Send(ref reply);
                PacketWriter w42 = new PacketWriter(0x2A);
                w42.WriteUInt32(Client.GameAccount.AccountId);
                w42.WriteInt32(0);
                w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
                w42.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
                w42.WriteByte(0x08);
                w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
                w42.WriteUInt32(slotId);
                w42.WriteByte(0x2F);
                w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
                w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);

                // These are item stats  ? figure them out
                //w42.WriteBytes(new byte[] { 0x2D, 0x00, 0x00, 0x00, 0x21, 0xFF });
                // sets max hp, use the chars current if no change
                w42.WriteUInt32((uint)Client.GameAccount.CurrentCharacter.TotalHP);

                w42.WriteBytes(new byte[] { 0x21, 0xFF });
                //w42.WriteBytes(new byte[] { 0x00, 0x12 }); // not sure
                //w42.WriteBytes(new byte[] { 0xA1, 0xF3 });
                w42.WriteBytes(new byte[4]);
                byte[] p42 = w42.ToArray();
                Client.Send(ref p42);
            }
        }
    }
}
