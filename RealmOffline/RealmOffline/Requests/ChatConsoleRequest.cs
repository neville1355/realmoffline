﻿using RealmOffline.Base;
using RealmOffline.Commands;
using RealmOffline.Core;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class ChatConsoleRequest : BaseWorldRequest {
        public ChatConsoleRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // Console.WriteLine("Start 37 : {0}", BitConverter.ToString(pak));
            // 0x80-8A are the emotes
            // This is the chat packet, it handles all chat as far as i can see
            //Console.WriteLine("Chat Packet {0}", Tools.NiceHexOutput(pak));
            //byte[] reply;
            // Other wise, lets read this 
            Reader.ReadBytes(4); //Pck Length
            Reader.ReadBytes(4); // Id
            int remaining = (int)(Reader.BaseStream.Length - Reader.BaseStream.Position + 5);
            try {
                List<byte> clean = new List<byte>();
                byte[] cb = Reader.ReadBytes(remaining);
                foreach (byte b in cb) {
                    if (b != 0x00) clean.Add(b);
                }
                string command = Encoding.ASCII.GetString(clean.ToArray());
                string[] cmd = command.Split(' ');
                // Good channel code, works fine
                // Need to rewrite this section
                // for custom commands.

                if (command.StartsWith("|c")) // room chat
                {
                    // Room Chat
                    // What room did we say this in ?
                    uint room = Client.GameAccount.CurrentCharacter.Location.RoomNumber;
                    List<Account> playersInRoom = ServerGlobals.GetAccountsInRoom(room);
                    //      Console.WriteLine("Total accounts in this room {0}", playersInRoom.Count);

                    for (int i = 0; i < playersInRoom.Count; i++) {
                        byte[] chat = chat = Packet.RoomChatPacket(Client.GameAccount,
                            playersInRoom[i].AccountId, clean.ToArray());
                        bool sent = ServerGlobals.TrySend(playersInRoom[i], chat);
                        if (!sent) Console.WriteLine("Failed to send room chat to account {0}", playersInRoom[i].AccountId);
                    }
                }
                else if (command.StartsWith("/join")) {
                    if (cmd.Length >= 2) {
                        // What channel ?
                        int channel = -1;
                        if (int.TryParse(cmd[1], out channel)) {
                            // try to find the channel
                            Channel c = null;
                            foreach (Channel chan in ScriptResolver.ImportedChannels) {
                                if (chan.ChannelNumber == cmd[1])
                                    c = chan;

                            }
                            if (c != null) {
                                // If player is already in a channel, make sure to leave it

                                if (cmd.Length == 3) {
                                    c.SendJoinPacket(Client.GameAccount, cmd[2]);
                                }
                                else
                                    c.SendJoinPacket(Client.GameAccount);
                                //   Console.WriteLine("Tried to join channel {0}", cmd[1]);
                            }
                            else {
                                // Force join 4
                                foreach (Channel c4 in ScriptResolver.ImportedChannels) {
                                    if (c4.ChannelNumber == "4")
                                        c4.SendJoinPacket(Client.GameAccount);
                                }
                                //   Console.WriteLine("Channel {0} not found.", cmd[1]);
                                //byte[] reply = Packet.ChatPacket(1, 1,
                                //   string.Format("Failed to locate channel #{0}.", cmd[1]), "Join");
                                //  Client.Send(ref reply);
                            }
                        }
                        else Console.WriteLine("We got a bad cmd parse.");
                    }
                }
                else if (command.StartsWith("/open")) {
                    //  Console.WriteLine("Wants to open group.");
                    PacketWriter w = new PacketWriter(0x26);
                    w.WriteUInt32(Client.GameAccount.AccountId);
                    w.WriteInt32(0);
                    w.WriteBytes(new byte[] { 0x2D, 0x33, 0x4F }); // maybe group id ?
                    string grp = "|c43|You are now accepting new group members.";
                    w.WriteBytes(Encoding.ASCII.GetBytes(grp));
                    w.WriteBytes(new byte[] { 0x0A, 0x00, 0x9D, 0x00 });
                    byte[] reply = w.ToArray();
                    Client.Send(ref reply);

                }
                else if (command.StartsWith("/close")) {
                    //   Console.WriteLine("Wants to close group.");
                    PacketWriter w = new PacketWriter(0x26);
                    w.WriteUInt32(Client.GameAccount.AccountId);
                    w.WriteInt32(0);
                    w.WriteBytes(new byte[] { 0x2D, 0x33, 0x43 }); // maybe group id ?
                    string grp = "|c43|You are no longer accepting new group members.";
                    w.WriteBytes(Encoding.ASCII.GetBytes(grp));
                    w.WriteBytes(new byte[] { 0x0A, 0x00 });
                    byte[] reply = w.ToArray();
                    Client.Send(ref reply);

                }
                else if (command.StartsWith("/gms")) {
                    // Do not know this packet, never any gms on.
                    PacketWriter w = new PacketWriter(0x3D);
                    w.WriteUInt32(Client.GameAccount.AccountId);
                    w.WriteInt32(0);
                    w.WriteShort(5);
                    w.WriteByte(0x00);
                    w.WriteByte(0x00);

                    byte[] reply = w.ToArray();
                    Client.Send(ref reply);
                    //    Console.WriteLine("Wants a gm list");
                }
                else if (command.StartsWith("/addfriend")) {
                    PacketWriter w = new PacketWriter(0x26);
                    w.WriteUInt32(Client.GameAccount.AccountId);
                    w.WriteInt32(0);
                    //w.WriteBytes(new byte[] { 0x2D, 0x66, 0x2B}); // Online
                    w.WriteBytes(new byte[] { 0x2D, 0x66, 0x20 }); // Offline
                    w.WriteBytes(Encoding.ASCII.GetBytes(cmd[1]));
                    w.WriteShort(0);
                    w.WriteByte(0xFF);
                    byte[] reply = w.ToArray();
                    Client.Send(ref reply);
                    //      Console.WriteLine("I wanna add friend {0}", cmd[1]);
                }
                else if (command.StartsWith("/delfriend")) {
                    PacketWriter w = new PacketWriter(0x26);
                    w.WriteUInt32(Client.GameAccount.AccountId);
                    w.WriteInt32(0);
                    w.WriteBytes(new byte[] { 0x2D, 0x66, 0x72 });
                    w.WriteBytes(Encoding.ASCII.GetBytes(cmd[1]));
                    w.WriteBytes(new byte[] { 0x00, 0x40, 0x01, 0xEE });
                    byte[] reply = w.ToArray();
                    Client.Send(ref reply);
                    //    Console.WriteLine("I wanna del friend {0}", cmd[1]);
                }
                else if (command.StartsWith("/ignore")) {
                    PacketWriter w = new PacketWriter(0x26);
                    w.WriteUInt32(Client.GameAccount.AccountId);
                    w.WriteInt32(0);
                    w.WriteBytes(new byte[] { 0x2D, 0x65, 0x20 }); //0x2B Online
                    w.WriteBytes(Encoding.ASCII.GetBytes(cmd[1]));
                    w.WriteBytes(new byte[] { 0x00, 0x00, 0x01, 0x48 });
                    byte[] reply = w.ToArray();
                    Client.Send(ref reply);
                    //    Console.WriteLine("I wanna ignore {0}", cmd[1]);
                }
                else if (command.StartsWith("/listen")) {
                    PacketWriter w = new PacketWriter(0x26);
                    w.WriteUInt32(Client.GameAccount.AccountId);
                    w.WriteInt32(0);
                    w.WriteBytes(new byte[] { 0x2D, 0x65, 0x72 });
                    w.WriteBytes(Encoding.ASCII.GetBytes(cmd[1]));
                    w.WriteBytes(new byte[] { 0x00, 0x40, 0x01, 0xEE });
                    byte[] reply = w.ToArray();
                    Client.Send(ref reply);
                    //     Console.WriteLine("I wanna unignore {0}", cmd[1]);
                }
                else if (command.StartsWith("/pignore")) {
                }
                else if (command.StartsWith("/report")) {
                }
                else if (command.StartsWith("/spawn")) {
                    new SpawnMobCommand(Client, Packets).SendResponse();
                }
                else if (command.StartsWith("/tp")) {
                    new TeleportToRoomCommand(Client, Packets).SendResponse();
                }
                else if (command.StartsWith("/")) {
                    OldCommand playerCommand = new OldCommand(Client.GameAccount, Packets);//new Command(Client.GameAccount, command);
                    playerCommand.ReactToCommand();
                }

            }
            catch { }
        }
    }
}
