﻿using RealmOffline.Accounts;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class EnterDoorRequest: BaseWorldRequest {
        public EnterDoorRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            //   Console.WriteLine("Start 95");
            // Use a doorish item
            Console.WriteLine("Dungeon/Door  Enter Item Use: {0}", BitConverter.ToString(Packets));
            Character c = Client.GameAccount.CurrentCharacter;
            Reader.ReadBytes(8);
            // what item id was used ?
            uint itemID = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            Reader.Close();
            // the id of the item, it is up to us to figure out where to go.

            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0x00);
            w25.WriteUInt32(0x5F);
            w25.WriteUInt32(0x00);
            w25.WriteByte(0x1A);
            w25.WriteByte(0x01);
            // Where they come out in the room
            w25.WriteUShort(Client.GameAccount.CurrentCharacter.Location.X);
            w25.WriteByte(0x03);
            w25.WriteByte(0x15);
            w25.WriteByte(0x01);
            // then they wanna walk to here.
            w25.WriteUShort(Client.GameAccount.CurrentCharacter.Location.Y);
            w25.WriteByte(0x01);
            w25.WriteUInt32(5043); // the room to go

            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);
            Console.WriteLine("Sent 95");
        }
    }
}
