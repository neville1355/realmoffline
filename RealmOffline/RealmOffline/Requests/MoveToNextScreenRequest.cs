﻿using RealmOffline.Accounts;
using RealmOffline.Core;
using RealmOffline.Core.Rooms;
using RealmOffline.Messages;
using RealmOffline.Tcp;
using RealmOffline.TCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class MoveToNextScreenRequest: BaseWorldRequest {
        public MoveToNextScreenRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Character c = Client.GameAccount.CurrentCharacter;
            Reader.ReadBytes(4);
            Reader.ReadBytes(4);
            ushort exitNum = BitConverter.ToUInt16(Reader.ReadBytes(2), 0);
            Reader.Close();

            // this is the exit we leaving by
            RealmOffline.Core.Rooms.Room.RoomExit e = Client.GameAccount.CurrentCharacter.Location.CurrentRoom.GetRoomExitByValue((byte)exitNum);
            uint newRoomNum = Client.GameAccount.CurrentCharacter.Location.CurrentRoom.GetNextRoomByExit(e);
            Room foundRoom = null;
            bool found = ScriptResolver.ImportedRooms.TryGetRoom(newRoomNum, out foundRoom);
            if (!found) found = ScriptResolver.ImportedRooms.TryGetRoom(5043, out foundRoom);

            //todo make this code a bit more transactional without side effects -> right now remove entity and add entity do extra stuff we don't know about
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RemoveEntity(Client.GameAccount.CurrentCharacter, Client.GameAccount);

            RemoveFromRoomMessage deleteMessage = new RemoveFromRoomMessage(
                Client.GameAccount, 
                Client.GameAccount.CurrentCharacter.Location.CurrentRoom, 
                Client.GameAccount.CurrentCharacter
            );
            RoomBroadcaster.SendToRoomExcludingSelf(Client.GameAccount.CurrentCharacter.Location.CurrentRoom, deleteMessage, Client.GameAccount);
            
            if (!foundRoom.Players.Contains(c)) foundRoom.Players.Add(c);
            //todo: delete this, ideally we remove these super objects and make it more transactional
            //foundRoom.AddEntity(Client.GameAccount.CurrentCharacter, Client.GameAccount);

            Client.GameAccount.CurrentCharacter.Location.CurrentRoom = foundRoom;

            AddToRoomMessage addMessage = new AddToRoomMessage(Client.GameAccount, c);
            RoomBroadcaster.SendToRoomExcludingSelf(foundRoom, addMessage, Client.GameAccount);
            Console.WriteLine("AddToRoomPacket {0}", string.Join(", ", addMessage.GetMessageBytes()));

            byte[] room = foundRoom.GetRoomPacket(Client.GameAccount.AccountId);
            Client.Send(ref room);
        }
    }
}
