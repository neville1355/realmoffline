﻿using RealmOffline.Accounts;
using RealmOffline.Core.Items.Base;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RealmOffline.Requests {
    class DropItemRequest: BaseWorldRequest {
        public DropItemRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Console.WriteLine("Packet 151 Drop Item: {0}", BitConverter.ToString(Packets));
            Reader.ReadBytes(8);
            Character c = Client.GameAccount.CurrentCharacter;

            uint itemNum = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            Reader.Close();
            
            // This all needs redone.
            //Client.GameAccount.CurrentCharacter.Location.CurrentRoom.DropItemInRoom(Client.GameAccount, itemNum);//SendPacket(reply42);

            BaseGameItem item = Client.GameAccount.CurrentCharacter.GetInventoryItem(itemNum);
            Client.GameAccount.CurrentCharacter.RemoveInventoryItem(item);
            // add it to room, this may crash loading now.
            Console.WriteLine("Trying to add item to room via addentity");
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.AddEntity(item, Client.GameAccount);


            // Get item from inventory
            // Now the packet 42 that does the dropping
            // item wrap
            PacketWriter w42A = new PacketWriter(0x2A);
            w42A.WriteUInt32(Client.GameAccount.AccountId);
            w42A.WriteUInt32(0x00);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42A.WriteByte(0x3F);
            w42A.WriteBytes(Packet.WrappedItem(Client.GameAccount, item));
            byte[] drop = w42A.ToArray();
            //Client.Send(ref drop);

            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(drop);
            // Asign item to rom
            item.AssignToRoom(Client.GameAccount.CurrentCharacter.Location);

            string fileLocation = Path.Combine(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location), "dropitem.txt");
            File.WriteAllText(fileLocation, BitConverter.ToString(drop));

            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteInt32(0x00);
            w25.WriteInt32(0x97);
            w25.WriteBytes(new byte[] { 0x65, 0x25, 0x05, 0x08 });
            byte[] reply25 = w25.ToArray();
            Client.Send(ref reply25);

            // Remove it from inventory
            //  bool rem = MySqlManager.DeleteItem(item.SqlID);


            PacketWriter w42B = new PacketWriter(0x2A);
            w42B.WriteUInt32(Client.GameAccount.AccountId);
            w42B.WriteUInt32(0x00);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42B.WriteByte(0x06);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42B.WriteUInt32(item.GameID);
            w42B.WriteByte(0x21);
            w42B.WriteByte(0xFF);
            byte[] p42B = w42B.ToArray();
            //Client.Send(ref p42B);
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(p42B);
            //Client.GameAccount.CurrentCharacter.Location.CurrentRoom.DropItemInRoom(Client.GameAccount, item.CurrentGameID);

            //  if (!rem) log.LogMessage(
            //    string.Format("Unable to remove item {0} from char {1}", itemNum, Client.GameAccount.SqlId), MessageType.Error);
            Console.WriteLine("Sent 151");
        }
    }
}
