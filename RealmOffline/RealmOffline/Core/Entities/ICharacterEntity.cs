﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Core.Entities {
    interface ICharacterEntity : IEntity {
        int Experience { get; }
        string Title { get; }
        byte Race { get; }
        byte Gender { get; }
        byte Girth { get; }
        byte Height { get; }
        byte XSpeed { get; }
        byte YSpeed { get; }
        byte PvpSwitch { get; }
        int CurrentHP { get; }
        int TotalHP { get; }

        //head parts
        byte RaceHead { get; set; }
        byte HeadPart { get; set; }
        byte ChinPart { get; set; }
        byte HairPart { get; set; }
        byte EyeBrowPart { get; set; }
        byte EyePart { get; set; }
        byte NosePart { get; set; }
        byte EarPart { get; set; }
        byte MouthPart { get; set; }
        byte FacialHairPart { get; set; }
        byte SkinColor { get; set; }
        byte HairColor { get; set; }
        byte EyeColor { get; set; }
    }
}
