﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Core
{
    public interface  IEntity
    {
        ushort ClassType { get; }
        byte Class { get; set; }
        uint GameID { get; set; }
        string Name { get; set; }
        bool IsHidden { get; set; }
        uint SittingOnGameID { get; set; }
        RoomLocation Location { get; set; }

        byte[] Serialize(out uint id);
        void Deserialize(byte[] data);
    }
}
