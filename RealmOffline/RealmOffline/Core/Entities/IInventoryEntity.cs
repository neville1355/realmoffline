﻿using RealmOffline.Core.Items.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Core.Entities {
    interface IInventoryEntity {
        uint InventoryId { get; set; }
        List<BaseGameItem> Inventory { get; }
    }
}
