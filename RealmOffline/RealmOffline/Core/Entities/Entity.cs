﻿using RealmOffline.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Core
{
    public abstract class Entity : IEntity
    {
        public abstract ushort GraphicID { get; set; }
        public abstract ushort Color { get; set; }        
        public RoomLocation Location { get; set; }
        public abstract byte[] Serialize(out uint itemID);
        public abstract void Deserialize(byte[] data);

        //IWorldObject members
        public abstract string Name { get; set; }
        public abstract uint GameID { get; set; }
        public ushort ClassType {
            get {
                return ClassTypes.Get(this.Class);
            }
        }
        public byte Class { get; set; }
        public bool IsHidden { get; set; }
        public uint SittingOnGameID { get; set; }
    }
}
