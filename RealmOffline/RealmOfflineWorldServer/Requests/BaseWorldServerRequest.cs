﻿using RealmOffline.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOfflineWorldServer.Requests {
    public abstract class BaseWorldServerRequest : BaseRequest {
        public BaseWorldServerRequest(byte[] packets) : base(packets) { }
    }
}
